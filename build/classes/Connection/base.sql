create database comptabiliter;
create role comptabiliter login  password 'comptabiliter';
alter database comptabiliter owner to comptabiliter;

\c comptabiliter comptabiliter;

create table journal(
    id serial primary key,
    nom varchar (10) not null
);
create table exercice(
    id serial primary key,
    datedebut date not null,
    datefin date not null
);
create table Ecriture(
    id serial primary key,
    idjournal int not null,
    idexercice int not null,
    daty date not null,
    foreign key (idjournal) references journal(id),
    foreign key (idexercice) references exercice(id)
);
create table compte(
    id serial primary key,
    compte int not null,
    libelle varchar(200) not null
);
create table SousEcriture(
    id serial primary key,
    idcompte int not null,
    idecriture int not null,
    reference varchar(100) not null,
    libelle varchar(200) not null,
    debit float not null,
    credit float not null,
    foreign key (idecriture) references Ecriture (id),
    foreign key (idcompte) references compte (id)
);
insert into exercice(datedebut,datefin) values ('2022/10/19','2023/10/18');
insert into journal(nom) values ('achat'),('vente'),('caisse'),('banque'),('divers');
insert into Ecriture(idjournal,idexercice,daty) values(1,2,'2022/10/19');
insert into SousEcriture(idcompte,idecriture,reference,libelle,debit,credit) values(1,2,'ati','scakc',2,0);

insert into compte (compte,intitule) values
(10100,'CAPITAL'),
(10610,'RESERVE LEGALE'),
(11000,'REPORT A NOUVEAU'),
(11010,'REPORT A NOUVEAU SOLDE CREDITEUR'),
(11200,'AUTRES PRODUITS ET CHARGES'),
(11900,'REPORT A NOUVEAU SOLDE DEBITEUR'),
(12800,'RESULTAT EN INSTANCE'),
(13300,'IMPOTS DIFFERES ACTIFS'),
(16110,'EMPRUNT A LT'),
(16510,'ENMPRUNT A MOYEN TERME'),
(20124,'FRAIS DE REHABILITATION'),
(20800,'AUTRES IMMOB INCORPORELLES'),
(21100,'TERRAINS'),
(21200,'CONSTRUCTION'),
(21300,'MATERIEL ET OUTILLAGE'),
(21510,'MATERIEL AUTOMOBILE'),
(21520,'MATERIEL MOTO'),
(21600,'AGENCEMENT AM INST'),
(21810,'MATERIELS ET MOBILIERS DE BUREAU'),
(21819,'MATERIELS INFORMATIQUES ET AUTRES'),
(21820,'MAT MOB DE LOGEMENT'),
(21880,'AUTRES IMMOBILISATIONS CORP'),
(23000,'IMMOBILISATION EN COURS'),
(28000,'AMORT IMMOB INCORP'),
(28120,'AMORTISSEMENT DES CONSTRUCTIONS'),
(28130,'AMORT MACH-MATER-OUTIL'),
(28150,'AMORT MAT DE TRANSPORT'),
(28160,'AMORT AAI'),
(28181,'AMORT MATERIELMOB'),
(28182,'AMORTISSEMENTS MATERIELS INFORMATIQ'),
(28183,'AMORT MATER  MOB LOGT'),
(32110,'STOCK MATIERES PREMIERES'),
(35500,'STOCK PRODUITS FINIS'),
(37000,'STOCK MARCHANDISES'),
(39700,'PROVISIONSDEPRECIATIONS STOCKS'),
(40110,'FOURNISSEURS DEXPLOITATIONS LOCAUX'),
(40120,'FOURNISSEURS DEXPLOITATIONS ETRANGERS'),
(40310,'FOURNISSEURS DIMMOBILISATION'),
(40810,'FRNS FACTURE A RECEVOIR'),
(40910,'FRNSAVANCESACOMPTES VERSER'),
(40980,'FRNS RABAIS A OBTENIR'),
(41110,'CLIENTS LOCAUX'),
(41120,'CLIENTS ETRANGERS'),
(41400,'CLIENTS DOUTEUX'),
(41800,'CLIENTS FACTURE A RETABLIR'),
(42100,'PERSONNEL SALAIRES A PAYER'),
(42510,'PERSONNEL AVANCES QUINZAINES'),
(42520,'PERSONNEL AVANCES SPECIALES'),
(42860,'PERSCHARGES  A PAYER'),
(43100,'CNAPS '),
(43120,'OSTIE'),
(44200,'ETAT IBS'),
(44210,'ACOMPTE IBS'),
(44321,'TVA IMPUTERDEC ULTERIEURE'),
(44500,'ETAT IRSA VERSER'),
(44560,'ETAT TVA DEDUCTIBLE'),
(44570,'ETAT TVA COLLECTEE'),
(44571,'TVA A VERSER'),
(45100,'COMPTE  COURANT ASSOC'),
(46700,'DEB CRED DIVERS'),
(46800,'CHARGES A PAYER DEBCRED DIVERS'),
(48610,'CHARGE CONSTATES DAVANCE'),
(49100,'PERTE CLIENTS'),
(51200,'BOA ANKORONDRANO'),
(51201,'BOA DOLLARS'),
(51202,'BNI MADAGASCAR'),
(51203,'BNI DOLLARS'),
(53100,'CAISSE '),
(58110,'VIREMENTINTERNEBANQCAISSE'),
(58130,'VIREMENT INTERNEBANQBANQ'),
(58140,'VIREMENT INTERNE CAISSECAISSE'),
(60100,'ACHAT MATIERES PREMIERESS'),
(60200,'FOURNIT DE MAGASIN'),
(60210,'FOURNIT BUR '),
(60220,'FOURNIT DE LOGT'),
(60230,'EMBALLAGES(PLAST-CARTON'),
(60240,'PIEC RECH VOITURES ADMINISTRATIVES'),
(60241,'PIEC RECH CAMIONS'),
(60242,'PIEC RECH MOTO'),
(60250,'AUTRES ACHATS'),
(60300,'VARIATION  STOCK MAT PREM'),
(60610,'EAU ET ELECTRICITE'),
(60620,'GAZ,COMBUST,CARBURANT,LUBRIF'),
(61300,'LOC IMMOBILIERES'),
(61380,'AUTRES LOCATIONS'),
(61550,'ENTRET  REP VEHICULE'),
(61560,'MAINTENANCE'),
(61610,'ASSURANCE GLOBALE DOMMAGES'),
(61611,'ASSURANCE FLOTTE AUTOMOBILE'),
(61800,'PHOTOCOPIE ET ASSIMILES '),
(61810,'ENVOI COLIS(LETTREDOC'),
(62100,'PERSONNEL EXTER'),
(62210,'HONORAIRE'),
(62220,'REMUNERATION DES TRANSITAIRES'),
(62230,'CATALOGUES ET IMPRIMES'),
(62240,'PUBLICATION'),
(62250,'SPONSORING-MECENAT'),
(62260,'TS DOUANE ET ASSIMILES'),
(62400,'FRAIS DE TRANSPORT'),
(62510,'VOYAGES   ET DEPLACEMENT'),
(62520,'MISSION(DEPL HEBERGT REST )'),
(62530,'RECEPTION'),
(62610,'SERVICES POSTAUX'),
(62620,'TELFAX'),
(62630,'INTERNET TANA'),
(62740,'COMMISSIONS BANCAIRE INTERNATIONALE'),
(62760,'COMMISSIONS BNI'),
(62770,'COMMISSIONS BOA'),
(62880,'AUTRES  CHARGES EXTERNES'),
(63680,'AUTRES IMPOTSTAXESDROITS DIV'),
(64100,'REMUNERATION PERSONNEL'),
(64120,'DROIT DE CONGES'),
(64511,'CNAPSCOTISATION  PATRONALE'),
(64512,'OSTIE  COTISATION PATRONALE'),
(64750,'MED ET ASSIM PERS'),
(65800,'AUTRES CHARGES DIVERSES'),
(65810,'ECARTPAIEMENT'),
(65811,'PERTETVA NON RECUPERABLE'),
(66200,'INTERETS  BANCAIRES BNI'),
(66300,'INTERETS  BANCAIRES BOA'),
(66600,'DIFFF  DE  CHANGE  PERTE'),
(66680,'AGIOSTRAITES'),
(68110,'DAP  IMMO INCORPORELLES'),
(68120,'DAP  IMMO  CORPORELLE'),
(70110,'VENTE LOCALE'),
(70120,'VENTES  A  L EXPORTATION'),
(70800,'AUTRES PROD  DES ACT ANNEXACS'),
(71300,'VARIATION DE STOCK  PF'),
(75800,'AUTRES PRODUITS D EXPLOITATION'),
(75810,'ECARTENCAISSEMENT'),
(76200,'INTERET CREDITEUR BANQUES BNI'),
(76300,'INTERET CREDITEUR BANQUES BOA'),
(76600,'DIFFERENCE DE  CHANGEPROFIT');


CREATE TABLE exercice(
    Id SERIAL,
    Debut DATE,
    Fin DATE,
    Etat INTEGER NOT NULL DEFAULT 0,
    PRIMARY KEY (Id)
);

CREATE TABLE mois(
    Id SERIAL,
    IdExercice INTEGER NOT NULL,
    Mois VARCHAR(15),
    PRIMARY KEY (Id)
);

CREATE TABLE journal(
    Id SERIAL,
    IdMois INTEGER NOT NULL,
    NomJournal VARCHAR(20),
    Etat INTEGER NOT NULL,
    PRIMARY KEY (Id),
    FOREIGN KEY (IdMois) REFERENCES mois(Id)
);

CREATE TABLE compte(
    Id SERIAL,
    Numero VARCHAR(20) NOT NULL,
    Intitule VARCHAR(50),
    PRIMARY KEY (Id)
);

CREATE TABLE tiers(
    Id SERIAL,
    IdCompte INTEGER NOT NULL,
    Intitule VARCHAR(50),
    Pseudo VARCHAR(50),
    PRIMARY KEY (Id) 
);

CREATE TABLE devise(
    Id SERIAL,
    TypeMoney VARCHAR(50),
    Montant DOUBLE PRECISION NOT NULL,
    PRIMARY KEY (Id)
);

CREATE TABLE ecriture(
    Id SERIAL,
    DateExercice DATE,
    piece VARCHAR(13),
    IdCompte INTEGER NOT NULL,
    Intitule VARCHAR(50),
    Libelle VARCHAR(50),
    IdTiers INTEGER NOT NULL,
    IdDevise INTEGER NOT NULL,
    DebitMontantEtranger DOUBLE PRECISION DEFAULT 0,
    CreditMontantEtranger DOUBLE PRECISION DEFAULT 0,
    Debit DOUBLE PRECISION DEFAULT 0,
    Credit DOUBLE PRECISION DEFAULT 0,
    FOREIGN KEY (IdCompte) REFERENCES compte(Id),
    FOREIGN KEY (IdTiers) REFERENCES tiers(Id),
    FOREIGN KEY (IdDevise) REFERENCES devise(Id)
);

CREATE TABLE ecritureBroullions(
    Id SERIAL,
    DateExercice DATE,
    piece VARCHAR(13),
    IdCompte INTEGER NOT NULL,
    Intitule VARCHAR(50),
    Libelle VARCHAR(50),
    IdTiers INTEGER NOT NULL,
    IdDevise INTEGER NOT NULL,
    DebitMontantEtranger DOUBLE PRECISION NOT NULL,
    CreditMontantEtranger DOUBLE PRECISION NOT NULL,
    Debit DOUBLE PRECISION NOT NULL,
    Credit DOUBLE PRECISION NOT NULL,
    FOREIGN KEY (IdCompte) REFERENCES compte(Id),
    FOREIGN KEY (IdTiers) REFERENCES tiers(Id),
    FOREIGN KEY (IdDevise) REFERENCES devise(Id)
);



insert into devise(TypeMoney,Montant)values('Euro',4500),
                                           ('Dollar',4000),
                                           ('yuang',3000);
                                           //('',4500),



insert into compte(Numero,Intitule)values(40200,'Fournisseur Rakoto'),
                                         (41200,'Client Rojo'),
                                         (52130,'BOA'),
                                         (60230,'achat Vary'),
                                         (70236,'Vente Bois');


insert into tiers(IdCompte,Intitule,Pseudo)values(1,'Rakoto','Rakoto Client'),
                                                 (2,'Rojo','Rojo Fournisseur');