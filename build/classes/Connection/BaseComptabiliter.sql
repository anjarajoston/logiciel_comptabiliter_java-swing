/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  User_HP
 * Created: 19 oct. 2022
 */

create database comptabiliter;
create role comptabiliter login  password 'comptabiliter';
alter database comptabiliter owner to comptabiliter;

\c comptabiliter comptabiliter;

create table exercice(
    id serial primary key,
    debut date,
    fin date,
    etat int default 0
);

insert into exercice(debut,fin) values('2022-10-01','2023-09-30');

create table journal(
    id serial primary key,
    nomJournal varchar(50)
);

insert into journal(nomJournal) values ('achat'),('vente'),('caisse'),('banque'),('divers');

create table mois(
    id serial primary key,
    mois varchar(50)
);

insert into mois(mois) values ('janvier'),('fevrier'),('mars'),('avril'),('mai'),('juin'),
('juillet'),('aout'),('septembre'),('octobre'),('novembre'),('decembre');

create table exercice_mois(
    idExercice int,
    idMois int,
    foreign key (idExercice) references exercice(id),
    foreign key (idMois) references mois(id)
);

create table compte(
    id serial primary key,
    compte int,
    intitule VARCHAR(200)
);

create table ecriture(
    id serial primary key,
    idjournal int,
    idexercice int,
    daty date,
    etat int default 1,
    foreign key (idjournal) references journal(id),
    foreign key (idexercice) references exercice(id)
);

create table sousEcriture(
    id serial primary key,
    idcompte int,
    idecriture int,
    intitule int,
    reference varchar(100),
    libelle varchar(200),
    debit double precision default 0,
    credit double precision default 0,
    etat int default 1,
    compte int,
    foreign key (idecriture) references ecriture(id),
    foreign key (idcompte) references compte(id)
);

create table validerEcriture(
    id serial primary key,
    idEcriture int,
    idUser int,
    dateAcceptation date,
    foreign key (idEcriture) references ecriture(id)
); 
