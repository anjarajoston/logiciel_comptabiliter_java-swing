/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comptabiter;

import Objet.ObjetBdd;
import java.sql.Date;

/**
 *
 * @author User_HP
 */
public class Exercice extends ObjetBdd {
    int id;
    Date debut;
    Date fin;
    int etat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDebut() {
        return debut;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    
    public int [] getMoisAnneeExercice(Exercice exercice){
        int [] listesMoisAnnee = new int[2];
        
        listesMoisAnnee[0] = Integer.parseInt(exercice.getDebut().toString().split("-")[1]);
        listesMoisAnnee[1] = Integer.parseInt(exercice.getDebut().toString().split("-")[0]);
        
        return listesMoisAnnee;
    }
}
