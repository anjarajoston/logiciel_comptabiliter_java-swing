/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comptabiter;


/**
 *
 * @author User_HP
 */
public class Ecriture {
    int compte;
    String debit;
    String credit;

    public int getCompte() {
        return compte;
    }

    public void setCompte(int compte) {
        this.compte = compte;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public Ecriture(int compte,String debit,String credit){
        setCompte(compte);
        setDebit(debit);
        setCredit(credit);
    }
    
    
}
