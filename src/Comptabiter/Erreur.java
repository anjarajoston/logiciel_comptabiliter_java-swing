/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comptabiter;

/**
 *
 * @author User_HP
 */
public class Erreur {
    boolean etat;
    String message;

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public Erreur(boolean etat,String message){
        setEtat(etat);
        setMessage(message);
    }
}
