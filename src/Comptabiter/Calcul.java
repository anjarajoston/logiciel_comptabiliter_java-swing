/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comptabiter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author User_HP
 */
public class Calcul {
    public int getDifference(String [] date,int enEm) throws Exception{
        int valeur = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date firstDate = sdf.parse(date[0]);
            Date secondDate = sdf.parse(date[1]);

            long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
            long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            
            if( enEm == 1 ){
                diff += 1;
            }
            
            valeur =  Integer.parseInt(((Long)diff).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valeur; 
    }
}
