/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comptabiter;

import Objet.ObjetBdd;

/**
 *
 * @author User_HP
 */
public class Journal extends ObjetBdd{
    int id;
    String nomjournal;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomjournal() {
        return nomjournal;
    }

    public void setNomjournal(String nomjournal) {
        this.nomjournal = nomjournal;
    }
}
