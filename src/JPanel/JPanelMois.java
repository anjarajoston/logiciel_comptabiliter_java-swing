/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Comptabiter.Exercice;
import Comptabiter.Mois;
import Listener.GetEcritureNonValiderListener;
import Listener.GoMoisListener;
import Listener.JButtonRetour;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author User_HP
 */
public class JPanelMois extends JPanel{
    Exercice exercice;
    
    public Exercice getExercice() {
        return exercice;
    }

    public void setExercice(Exercice exercice) {
        this.exercice = exercice;
    }
    public JPanelMois(Exercice exercice) throws Exception{
        this.setLayout(null);
        setExercice(exercice);
        
        
        JLabel labelExercice = new JLabel();
        labelExercice.setText("idExercice : "+((Integer)getExercice().getId()).toString());
        labelExercice.reshape(1, 1, 300, 50);
        this.add(labelExercice);
        JLabel labelMois = new JLabel();
        labelMois.setText(" Listes mois : ");
        labelMois.reshape(120, 1, 100, 50);
        this.add(labelMois);
        Connection c = null;
        Mois mois = new Mois();
        ArrayList<Object> listesMois = mois.find(c);
        int a = 20;
        int b = 30;
        for ( int i = 0 ; i < listesMois.size() ; i++ ){
            JLabel libelleMois = new JLabel();
            libelleMois.reshape(140, a, 100, 50);
            JButton goMois = new JButton();
            goMois.reshape(220, b, 80, 30);
            goMois.setText("Choisir");
            libelleMois.setText(((Mois)listesMois.get(i)).getMois());
            goMois.addActionListener(new GoMoisListener(getExercice(),(Mois)listesMois.get(i)));
            this.add(libelleMois);
            this.add(goMois);
            a+= 35;
            b+= 35;
        }
        
        /*-------------------------------- Lister Ecriture non valider --------------------------------------*/
        JButton getEcriture = new JButton();
        getEcriture.reshape(450, 100, 250, 30);
        getEcriture.setText("Voir listes des écritures non valider");
        getEcriture.addActionListener(new GetEcritureNonValiderListener());
        this.add(getEcriture);
        /*----------------------------------------------------------------------*/
        
        this.add(new JButtonRetour());
    }
}
