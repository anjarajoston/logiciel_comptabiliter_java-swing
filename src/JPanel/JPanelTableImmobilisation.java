/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Compte.Immobilisation;
import Listener.GetMontantParDateListener;
import Listener.JButtonRetour;
import java.sql.Date;
import java.time.LocalDate;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author User_HP
 */
public class JPanelTableImmobilisation extends JPanel{
    
    public JPanelTableImmobilisation(JTable tableAmmortissement,Immobilisation immo,String date) throws Exception {
        try {
            this.setLayout(null);
        /*--------------------------------- Insert Annee -----------------------------------*/
            JLabel labelAnnee = new JLabel();
            labelAnnee.setText(" Annee : ");
            labelAnnee.reshape(1, 1, 60, 30);
            this.add(labelAnnee);
            
            JTextField textFieldLibelle = new JTextField();
            textFieldLibelle.reshape(80, 1, 150, 25);
            this.add(textFieldLibelle);
            
            JButton buttonGetMontant = new JButton();
            buttonGetMontant.setText(" GetMontant ");
            buttonGetMontant.reshape(300, 1, 150, 25);
            buttonGetMontant.addActionListener(new GetMontantParDateListener(textFieldLibelle,immo,Date.valueOf(LocalDate.now()).toString()));
            this.add(buttonGetMontant);
        
        /*--------------------------------------------------------------------*/
            
            JScrollPane paneCapitaux = new JScrollPane(tableAmmortissement);
            paneCapitaux.reshape(1, 50, 1200, 300);
            this.add(paneCapitaux);
            
            if( immo.getEtat() ){
                JLabel labelDate = new JLabel();
                labelDate.setText("Date demander : ");
                labelDate.reshape(1, 400, 150, 30);
                this.add(labelDate);
                
                JTextField dateDemande = new JTextField();
                dateDemande.setText(date);
                dateDemande.reshape(100, 400, 80, 25);
                this.add(dateDemande);
                
                JLabel labelMontantFinal = new JLabel();
                labelMontantFinal.setText("Montant Final : ");
                labelMontantFinal.reshape(300, 400, 150, 30);
                this.add(labelMontantFinal);
                
                JTextField montantFinal = new JTextField();
                if( immo.getMontantVente() < 0 ){
                    montantFinal.setText("0.0 Ar");
                } else {
                    montantFinal.setText(((Double)immo.getMontantVente()).toString()+" Ar");
                }
                montantFinal.reshape(400, 400, 200, 25);
                this.add(montantFinal);
            }
            
            this.add(new JButtonRetour());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
