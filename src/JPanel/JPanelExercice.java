/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Comptabiter.Exercice;
import Listener.GoBalanceListener;
import Listener.GoExerciceListener;
import Listener.GoImmobilisationListener;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author User_HP
 */
public class JPanelExercice extends JPanel{
    JFrame frame;

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }
    
    public JPanelExercice() throws Exception{
        this.setLayout(null);
        JLabel labelMois = new JLabel();
        labelMois.setText(" Listes exercice : ");
        labelMois.reshape(10, 1, 100, 50);
        this.add(labelMois);
        JLabel debut = new JLabel();
        debut.reshape(100, 20, 150, 50);
        debut.setText("DEBUT");
        this.add(debut);
        JLabel fin = new JLabel();
        fin.reshape(200, 20, 150, 50);
        fin.setText("FIN");
        this.add(fin);
        Connection c = null;
        Exercice exercice = new Exercice();
        ArrayList<Object> listesExercices = exercice.find(c);
        int a = 40;
        int b = 50;
        for ( int i = 0 ; i < listesExercices.size() ; i++ ){
            JLabel labelDebut = new JLabel();
            labelDebut.reshape(100, a, 150, 50);
            JLabel labelFin = new JLabel();
            labelFin.reshape(200, a, 150, 50);
            JButton goExercice = new JButton();
            goExercice.reshape(300, b, 80, 30);
            goExercice.setText("Choisir");
            labelDebut.setText(((Exercice)listesExercices.get(i)).getDebut().toString());
            labelFin.setText(((Exercice)listesExercices.get(i)).getFin().toString());
            goExercice.addActionListener(new GoExerciceListener((Exercice)listesExercices.get(i)));
            this.add(labelDebut);
            this.add(labelFin);
            this.add(goExercice);
            a += 35;
            b += 35;
        }
        
        JButton balance = new JButton();
        balance.reshape(410, 50, 150, 30);
        balance.setText("Balance");
        balance.addActionListener(new GoBalanceListener());
        this.add(balance);
        
        JButton immobilisation = new JButton();
        immobilisation.reshape(600, 50, 150, 30);
        immobilisation.setText("Immobilisation");
        immobilisation.addActionListener(new GoImmobilisationListener());
        this.add(immobilisation);
    }
}
