/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Comptabiter.Compte;
import Listener.GoAmmortissementListener;
import Listener.JButtonRetour;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *ssss
 * @author User_HP
 */
public class JPanelImmobilisation extends JPanel{
    public JPanelImmobilisation() throws Exception {
        try {
            this.setLayout(null);
            
            JLabel label = new JLabel();
            label.setText(" Insertion immobilisation : ");
            label.reshape(10, 50, 200, 30);
            this.add(label);
            
            /*===========================*/
            JLabel labelCompte = new JLabel();
            labelCompte.setText(" Compte : ");
            labelCompte.reshape(1, 1, 60, 30);
            this.add(labelCompte);
            Connection c = null;
            Compte compte = new Compte();
            compte.setNumerocompte(4);
            System.out.println(compte.getRequetFind(compte));
            ArrayList<Object> listesComptes = compte.find(c);
            JComboBox compteCombo = new JComboBox();
            for( int i = 0 ; i < listesComptes.size() ; i++ ){
                compteCombo.addItem((Integer)((Compte)listesComptes.get(i)).getCompte()+" "+((Compte)listesComptes.get(i)).getIntitule());
            }
            compteCombo.reshape(80, 1, 300, 25);
            this.add(compteCombo);
            /*===========================*/
            
        /*------------------------------- Libelle immo -------------------------------------*/
            
            JLabel libelle = new JLabel();
            libelle.setText(" Libelle ");
            libelle.reshape(200, 20, 200, 30);
            this.add(libelle);
            
            JTextField textFieldLibelle = new JTextField();
            textFieldLibelle.reshape(175, 50, 100, 30);
            this.add(textFieldLibelle);
            
        /*--------------------------------------------------------------------*/
        
        /*--------------------------------- Montant immo -----------------------------------*/
        
            JLabel montant = new JLabel();
            montant.setText(" Montant ");
            montant.reshape(300, 20, 200, 30);
            this.add(montant);
            
            JTextField textFieldMontant = new JTextField();
            textFieldMontant.reshape(275, 50, 100, 30);
            this.add(textFieldMontant);
            
        /*--------------------------------------------------------------------*/
        
        /*--------------------------------- Annee ammortissement immo -----------------------------------*/
        
            JLabel ammortissement = new JLabel();
            ammortissement.setText(" Ammortissement ");
            ammortissement.reshape(400, 20, 200, 30);
            this.add(ammortissement);
            
            JTextField textFieldAmmortissement = new JTextField();
            textFieldAmmortissement.reshape(375, 50, 150, 30);
            this.add(textFieldAmmortissement);
            
        /*--------------------------------------------------------------------*/
        
        /*--------------------------------- Date ammortissement immo -----------------------------------*/
        
            JLabel date = new JLabel();
            date.setText(" Date ");
            date.reshape(575, 20, 200, 30);
            this.add(date);
            
            JTextField textFieldDate = new JTextField();
            textFieldDate.reshape(525, 50, 150, 30);
            this.add(textFieldDate);
            
        /*--------------------------------------------------------------------*/
        
        /*---------------------------------- Button Insert Ammortissement ----------------------------------*/
            
            JButton goImmobilisation = new JButton();
            goImmobilisation.reshape(675, 49, 100, 30);
            goImmobilisation.setText("Inserer");
            goImmobilisation.addActionListener(new GoAmmortissementListener(textFieldLibelle,textFieldMontant,textFieldAmmortissement,textFieldDate));
            this.add(goImmobilisation);
        
        /*--------------------------------------------------------------------*/
        
        
            this.add(new JButtonRetour());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
