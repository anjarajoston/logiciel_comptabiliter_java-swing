/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Listener.JButtonRetour;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author User_HP
 */
public class JPanelBalance extends JPanel{
    public JPanelBalance(JTable tableCapitaux,JTable tableImmobilisation,JTable tableStock,JTable tableFournisseur,JTable tableBanque,JTable tableAchat,JTable tableVente) throws Exception{
        try {
            this.setLayout(null);
            JScrollPane paneCapitaux = new JScrollPane(tableCapitaux);
            paneCapitaux.reshape(50, 0, 900, 120);
            this.add(paneCapitaux);
            
            JScrollPane paneImmobilisation = new JScrollPane(tableImmobilisation);
            paneImmobilisation.reshape(50, 100, 900, 120);
            this.add(paneImmobilisation);
            
            JScrollPane paneStock = new JScrollPane(tableStock);
            paneStock.reshape(50, 200, 900, 120);
            this.add(paneStock);
            
            JScrollPane paneFournisseur = new JScrollPane(tableFournisseur);
            paneFournisseur.reshape(50, 300, 900, 120);
            this.add(paneFournisseur);
            
            JScrollPane paneBanque = new JScrollPane(tableBanque);
            paneBanque.reshape(50, 400, 900, 120);
            this.add(paneBanque);
            
            JScrollPane paneAchat = new JScrollPane(tableAchat);
            paneAchat.reshape(50, 500, 900, 120);
            this.add(paneAchat);
            
            JScrollPane paneVente = new JScrollPane(tableVente);
            paneVente.reshape(50, 600, 900, 120);
            this.add(paneVente);
            
            JButtonRetour buttonRetour = new JButtonRetour();
            buttonRetour.reshape(1000, 1, 80, 30);
            this.add(buttonRetour);
        } catch ( Exception ex ) {
            System.out.println(ex.getMessage());
        }
    }
}
