/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author User_HP
 */
public class JPanelErreur extends JPanel{
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public JPanelErreur(String message){
        this.setLayout(null);
        setMessage(message);
        JLabel label = new JLabel();
        label.setText(getMessage());
        label.reshape(1, 1, 500, 50);
        this.add(label);
    }
}
