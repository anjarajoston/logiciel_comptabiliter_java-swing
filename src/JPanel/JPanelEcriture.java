/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Comptabiter.Compte;
import Comptabiter.Exercice;
import Comptabiter.Journal;
import Comptabiter.Mois;
import Fille.SousEcriture;
import Listener.GoSauvegarde;
import Listener.JButtonRetour;
import Listener.ValidationListener;
import Mere.Ecriture;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author User_HP
 */
public class JPanelEcriture extends JPanel{
    Exercice exercice;
    Mois mois;
    Journal journal;
    
    public Exercice getExercice() {
        return exercice;
    }

    public void setExercice(Exercice exercice) {
        this.exercice = exercice;
    }

    public Mois getMois() {
        return mois;
    }

    public void setMois(Mois mois) {
        this.mois = mois;
    }

    public Journal getJournal() {
        return journal;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }
    public JPanelEcriture(int jour,Exercice exercice,Mois mois,Journal journal) throws Exception {
        this.setLayout(null);
        setExercice(exercice);
        setMois(mois);
        setJournal(journal);
        
        /*--------------------------------- Instanciation de l'objet Ecriture en cours ----------------------------------------*/
        Ecriture ecriture = new Ecriture();
        ecriture.setIdexercice(getExercice().getId());
        ecriture.setIdjournal(getJournal().getId());
        String date = ecriture.getDateEcriture(exercice, mois, jour);
        ecriture.setDaty(Date.valueOf(date));
        ecriture.setEtat(1);
        JLabel labelEcriture = new JLabel();
        labelEcriture.setText(" Ecriture en cours : idExercice = "+ecriture.getIdexercice()+" , idJournal = "+ecriture.getIdjournal()+" , date du mouvement = "+ecriture.getDaty());
        labelEcriture.reshape(1, 20, 600, 50);
        this.add(labelEcriture);
        /*-------------------------------------------------------------------------*/

        /*----------------------------------- Insertion Sous-Ecriture --------------------------------------*/
        JLabel labelJour = new JLabel();
        labelJour.setText("jour:");
        labelJour.reshape(1, 50, 60, 50);
        this.add(labelJour);
        JComboBox comboJour = new JComboBox();
        for( int i = 1 ; i < 31 ; i++ ){
            comboJour.addItem(i);
        }
        comboJour.reshape(30, 60, 40, 30);
        this.add(comboJour);
        /*===========================*/
        JLabel labelCompte = new JLabel();
        labelCompte.setText("compte:");
        labelCompte.reshape(70, 50, 60, 50);
        this.add(labelCompte);
        Connection c = null;
        Compte compte = new Compte();
        ArrayList<Object> listesComptes = compte.find(c);
        JComboBox compteCombo = new JComboBox();
        for( int i = 0 ; i < listesComptes.size() ; i++ ){
            compteCombo.addItem((Integer)((Compte)listesComptes.get(i)).getCompte());
        }
        compteCombo.reshape(120, 60, 60, 30);
        this.add(compteCombo);
        /*===========================*/
        JLabel labelReference = new JLabel();
        labelReference.setText("reference:");
        labelReference.reshape(180, 50, 80, 50);
        this.add(labelReference);
        JTextField reference = new JTextField();
        reference.reshape(240, 60, 80, 30);
        this.add(reference);
        /*===========================*/
        JLabel labelDebit = new JLabel();
        labelDebit.setText("debit:");
        labelDebit.reshape(320, 50, 60, 50);
        this.add(labelDebit);
        JTextField debit = new JTextField();
        debit.reshape(350, 60, 60, 30);
        this.add(debit);
        
        JLabel labelCredit = new JLabel();
        labelCredit.setText("credit : ");
        labelCredit.reshape(410, 50, 60, 50);  
        this.add(labelCredit);
        JTextField credit = new JTextField();
        credit.reshape(450, 60, 60, 30);
        this.add(credit);
        /*===========================*/
        JButton goExercice = new JButton();
        goExercice.reshape(520, 60, 100, 30);
        goExercice.setText("Inserer");
        this.add(goExercice);
        GoSauvegarde save = new GoSauvegarde(ecriture,compteCombo,reference,debit,credit,this);
        goExercice.addActionListener(save);
        /*==============================*/
        JLabel ecritureColonne = new JLabel();
        ecritureColonne.setText("|ecriture");
        ecritureColonne.reshape(60, 100, 100, 20);
        this.add(ecritureColonne);
        JLabel labelIntitule = new JLabel();
        labelIntitule.setText("|intitule");
        labelIntitule.reshape(120, 100, 100, 20);
        this.add(labelIntitule);
        JLabel labelLibelle = new JLabel();
        labelLibelle.setText("|libelle");
        labelLibelle.reshape(180, 100, 260, 20);
        this.add(labelLibelle);
        JLabel colonneReference = new JLabel();
        colonneReference.setText("|reference");
        colonneReference.reshape(450, 100, 100, 20);
        this.add(colonneReference);
        JLabel labelValDebit = new JLabel();
        labelValDebit.setText("|debit");
        labelValDebit.reshape(560, 100, 50, 20);  
        this.add(labelValDebit);
        JLabel labelValCredit = new JLabel();
        labelValCredit.setText("|credit");
        labelValCredit.reshape(650, 100, 50, 20);  
        this.add(labelValCredit);
        
        System.out.println(ecriture.getRequetFind(ecriture));
        ArrayList<Object> newListeEcriture = ecriture.find(c);
        ecriture.setTableFille("sousecriture");
        ecriture.setLiaison("idecriture");
        SousEcriture sousEcriture = new SousEcriture();
        ecriture.setFille(sousEcriture);
        
        if( newListeEcriture.size() != 0 ){
            ecriture.setId(((Ecriture)newListeEcriture.get(0)).getId());
            ecriture.setIdMere(ecriture.getId());
            
            Vector tableauxFilles = ecriture.getTableauFille(c);
            
            double [] tableauSommeDebitCredit = ecriture.getSommeDebitCredit(ecriture);
            
            int y = 120;
            int v = 0;
            int x = 0;
            for( int i = 0 ; i < tableauxFilles.size() ; i++ ){
                JTextField val1 = new JTextField(((Integer)(((SousEcriture)tableauxFilles.elementAt(i)).getIdEcriture())).toString());
                val1.reshape(60, y, 50, 20);
                this.add(val1);
                JTextField val2 = new JTextField(((Integer)(((SousEcriture)tableauxFilles.elementAt(i)).getIntitule())).toString());
                val2.reshape(120, y, 50, 20);
                this.add(val2);
                JTextField val3 = new JTextField(((SousEcriture)tableauxFilles.elementAt(i)).getLibelle());
                val3.reshape(180, y, 260, 20);
                this.add(val3);
                JTextField val4 = new JTextField(((SousEcriture)tableauxFilles.elementAt(i)).getReference());
                val4.reshape(450, y, 100, 20);
                this.add(val4);
                JTextField val5 = new JTextField(((Double)((SousEcriture)tableauxFilles.elementAt(i)).getDebit()).toString());
                val5.reshape(560, y, 80, 20);  
                this.add(val5);
                JTextField val6 = new JTextField(((Double)((SousEcriture)tableauxFilles.elementAt(i)).getCredit()).toString());
                val6.reshape(650, y, 80, 20);  
                this.add(val6);

                y+=20;
                
                v=y;
                x=v+20;
            }
            JTextField sommeDebit = new JTextField(((Double)(tableauSommeDebitCredit[0])).toString());
            sommeDebit.reshape(560, v, 80, 20);  
            this.add(sommeDebit);
            JTextField sommeCredit = new JTextField(((Double)(tableauSommeDebitCredit[1])).toString());
            sommeCredit.reshape(650, v, 80, 20);  
            this.add(sommeCredit);
            JButton validerEcriture = new JButton();
            validerEcriture.reshape(600, x, 80, 30);
            validerEcriture.setText("Valider");
            this.add(validerEcriture);
            validerEcriture.addActionListener(new ValidationListener(ecriture,this));
        }
        
        /*===========================*/
       
        this.add(new JButtonRetour());
    }
    
    public JPanelEcriture(Ecriture ecriture) throws Exception {
        try{
            this.setLayout(null);

            /*--------------------------------- Instanciation de l'objet Ecriture en cours ----------------------------------------*/
            JLabel labelEcriture = new JLabel();
            labelEcriture.setText(" Ecriture en cours : idExercice = "+ecriture.getIdexercice()+" , idJournal = "+ecriture.getIdjournal()+" , date du mouvement = "+ecriture.getDaty());
            labelEcriture.reshape(1, 20, 600, 50);
            this.add(labelEcriture);
            /*-------------------------------------------------------------------------*/

            /*----------------------------------- Insertion Sous-Ecriture --------------------------------------*/
            JLabel labelJour = new JLabel();
            labelJour.setText("jour:");
            labelJour.reshape(1, 50, 60, 50);
            this.add(labelJour);
            JComboBox comboJour = new JComboBox();
            for( int i = 1 ; i < 31 ; i++ ){
                comboJour.addItem(i);
            }
            comboJour.reshape(30, 60, 40, 30);
            this.add(comboJour);
            /*===========================*/
            JLabel labelCompte = new JLabel();
            labelCompte.setText("compte:");
            labelCompte.reshape(70, 50, 60, 50);
            this.add(labelCompte);
            Connection c = null;
            Compte compte = new Compte();
            ArrayList<Object> listesComptes = compte.find(c);
            JComboBox compteCombo = new JComboBox();
            for( int i = 0 ; i < listesComptes.size() ; i++ ){
                compteCombo.addItem((Integer)((Compte)listesComptes.get(i)).getCompte());
            }
            compteCombo.reshape(120, 60, 60, 30);
            this.add(compteCombo);
            /*===========================*/
            JLabel labelReference = new JLabel();
            labelReference.setText("reference:");
            labelReference.reshape(180, 50, 80, 50);
            this.add(labelReference);
            JTextField reference = new JTextField();
            reference.reshape(240, 60, 80, 30);
            this.add(reference);
            /*===========================*/
            JLabel labelDebit = new JLabel();
            labelDebit.setText("debit:");
            labelDebit.reshape(320, 50, 60, 50);
            this.add(labelDebit);
            JTextField debit = new JTextField();
            debit.reshape(350, 60, 60, 30);
            this.add(debit);

            JLabel labelCredit = new JLabel();
            labelCredit.setText("credit : ");
            labelCredit.reshape(410, 50, 60, 50);  
            this.add(labelCredit);
            JTextField credit = new JTextField();
            credit.reshape(450, 60, 60, 30);
            this.add(credit);
            /*===========================*/
            JButton goExercice = new JButton();
            goExercice.reshape(520, 60, 100, 30);
            goExercice.setText("Inserer");
            this.add(goExercice);
            GoSauvegarde save = new GoSauvegarde(ecriture,compteCombo,reference,debit,credit,this);
            goExercice.addActionListener(save);
            /*==============================*/
            JLabel ecritureColonne = new JLabel();
            ecritureColonne.setText("|ecriture");
            ecritureColonne.reshape(60, 100, 100, 20);
            this.add(ecritureColonne);
            JLabel labelIntitule = new JLabel();
            labelIntitule.setText("|intitule");
            labelIntitule.reshape(120, 100, 100, 20);
            this.add(labelIntitule);
            JLabel labelLibelle = new JLabel();
            labelLibelle.setText("|libelle");
            labelLibelle.reshape(180, 100, 260, 20);
            this.add(labelLibelle);
            JLabel colonneReference = new JLabel();
            colonneReference.setText("|reference");
            colonneReference.reshape(450, 100, 100, 20);
            this.add(colonneReference);
            JLabel labelValDebit = new JLabel();
            labelValDebit.setText("|debit");
            labelValDebit.reshape(560, 100, 50, 20);  
            this.add(labelValDebit);
            JLabel labelValCredit = new JLabel();
            labelValCredit.setText("|credit");
            labelValCredit.reshape(650, 100, 50, 20);  
            this.add(labelValCredit);

            System.out.println(ecriture.getRequetFind(ecriture));
            ArrayList<Object> newListeEcriture = ecriture.find(c);
            
            ecriture.setTableFille("sousecriture");
            ecriture.setLiaison("idecriture");
            SousEcriture sousEcriture = new SousEcriture();
            ecriture.setFille(sousEcriture);

            if( !newListeEcriture.isEmpty() ){
                ecriture.setId(((Ecriture)newListeEcriture.get(0)).getId());
                ecriture.setIdMere(ecriture.getId());
                
                Vector tableauxFilles = ecriture.getTableauFille(c);

                double [] tableauSommeDebitCredit = ecriture.getSommeDebitCredit(ecriture);

                int y = 120;
                int v = 0;
                int x = 0;
                for( int i = 0 ; i < tableauxFilles.size() ; i++ ){
                    JTextField val1 = new JTextField(((Integer)(((SousEcriture)tableauxFilles.elementAt(i)).getIdEcriture())).toString());
                    val1.reshape(60, y, 50, 20);
                    this.add(val1);
                    JTextField val2 = new JTextField(((Integer)(((SousEcriture)tableauxFilles.elementAt(i)).getIntitule())).toString());
                    val2.reshape(120, y, 50, 20);
                    this.add(val2);
                    JTextField val3 = new JTextField(((SousEcriture)tableauxFilles.elementAt(i)).getLibelle());
                    val3.reshape(180, y, 260, 20);
                    this.add(val3);
                    JTextField val4 = new JTextField(((SousEcriture)tableauxFilles.elementAt(i)).getReference());
                    val4.reshape(450, y, 100, 20);
                    this.add(val4);
                    JTextField val5 = new JTextField(((Double)((SousEcriture)tableauxFilles.elementAt(i)).getDebit()).toString());
                    val5.reshape(560, y, 80, 20);  
                    this.add(val5);
                    JTextField val6 = new JTextField(((Double)((SousEcriture)tableauxFilles.elementAt(i)).getCredit()).toString());
                    val6.reshape(650, y, 80, 20);  
                    this.add(val6);

                    y+=20;

                    v=y;
                    x=v+20;
                }
                JTextField sommeDebit = new JTextField(((Double)(tableauSommeDebitCredit[0])).toString());
                sommeDebit.reshape(560, v, 80, 20);  
                this.add(sommeDebit);
                JTextField sommeCredit = new JTextField(((Double)(tableauSommeDebitCredit[1])).toString());
                sommeCredit.reshape(650, v, 80, 20);  
                this.add(sommeCredit);
                JButton validerEcriture = new JButton();
                validerEcriture.reshape(600, x, 80, 30);
                validerEcriture.setText("Valider");
                this.add(validerEcriture);
                validerEcriture.addActionListener(new ValidationListener(ecriture,this));
            }

            /*===========================*/
            
            try {
                if( ecriture.testValidationEcriture(ecriture).getEtat() ){
                    JButtonRetour buttonRetour = new JButtonRetour();
                    buttonRetour.setEtat(true);
                    this.add(buttonRetour);
                }
            } catch (Exception e) {
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
