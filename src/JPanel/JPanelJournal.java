/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Comptabiter.Exercice;
import Comptabiter.Journal;
import Comptabiter.Mois;
import Listener.GoJournalListener;
import Listener.JButtonRetour;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author User_HP
 */
public class JPanelJournal extends JPanel{
    Exercice exercice;
    Mois mois;

    public Exercice getExercice() {
        return exercice;
    }

    public void setExercice(Exercice exercice) {
        this.exercice = exercice;
    }

    public Mois getMois() {
        return mois;
    }

    public void setMois(Mois mois) {
        this.mois = mois;
    }
    
    public JPanelJournal(Exercice exercice,Mois mois) throws Exception{
        this.setLayout(null);
        setExercice(exercice);
        setMois(mois);
        
        //System.out.println(mois.getMoisAnneeEcriture(exercice, mois));
        JLabel labelJour = new JLabel();
        labelJour.setText("Jour du mouvement : ");
        labelJour.reshape(120, 1, 300, 50);
        this.add(labelJour);
        JComboBox jour = new JComboBox();
        for( int i = 1 ; i < 31 ; i++ ){
            jour.addItem(i);
        }
        jour.reshape(250, 5, 40, 30);
        this.add(jour);
        JLabel lableExercice = new JLabel();
        lableExercice.setText("idExercice : "+((Integer)exercice.getId()).toString());
        lableExercice.reshape(1, 1, 300, 50);
        this.add(lableExercice);
        JLabel labelMois = new JLabel();
        labelMois.setText("idMois : "+((Integer)mois.getId()).toString());
        labelMois.reshape(1, 21, 300, 50);
        this.add(labelMois);
        JLabel labelJournal = new JLabel();
        labelJournal.setText(" Listes journal : ");
        labelJournal.reshape(120, 41, 100, 50);
        this.add(labelJournal);
        Connection c = null;
        Journal journal = new Journal();
        ArrayList<Object> listesJournals = journal.find(c);
        int a = 60;
        int b = 70;
        for ( int i = 0 ; i < listesJournals.size() ; i++ ){
            JLabel libelleJournal = new JLabel();
            libelleJournal.reshape(140, a, 100, 50);
            JButton goJournal = new JButton();
            goJournal.reshape(220, b, 80, 30);
            goJournal.setText("Choisir");
            libelleJournal.setText(((Journal)listesJournals.get(i)).getNomjournal());
            goJournal.addActionListener(new GoJournalListener(jour,exercice,mois,(Journal)listesJournals.get(i)));
            this.add(libelleJournal);
            this.add(goJournal);
            a += 35;
            b += 35;
        }
        this.add(new JButtonRetour());
    }
}
