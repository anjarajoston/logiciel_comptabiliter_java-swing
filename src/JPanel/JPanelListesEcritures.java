/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPanel;

import Listener.GoJournalListener;
import Listener.JButtonRetour;
import Listener.SaveEcritureToValideListener;
import Listener.ValidationPlusieursEcrituresListener;
import Mere.Ecriture;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author User_HP
 */
public class JPanelListesEcritures extends JPanel{
    public JPanelListesEcritures() throws Exception {
        try {
            this.setLayout(null);
            Ecriture ecriture = new Ecriture();
            ecriture.setEtat(1);
            String [] enTete = new String[3];
            enTete[0] = "idjournal";enTete[1] = "idexercice";enTete[2] = "daty";
            ecriture.setEnTete(enTete);
            JTable tableEcriture = ecriture.lister(ecriture);
            JScrollPane pane = new JScrollPane(tableEcriture);
            pane.reshape(100, 100, 500, 100);
            this.add(pane);
            
            
            JLabel labelValidation = new JLabel();
            labelValidation.setText("Valider");
            labelValidation.reshape(630, 100, 80, 20);
            this.add(labelValidation);
            
            int y = 119;
            
            ArrayList<Object> listesEcriture = ecriture.find(null);
            
            Vector<Ecriture> vectorEcriture = new Vector();
            for( int i = 0 ; i < listesEcriture.size() ; i++ ){
                JCheckBox checkBox = new JCheckBox();
                checkBox.reshape(640, y, 20, 20);
                SaveEcritureToValideListener save = new SaveEcritureToValideListener(((Ecriture)listesEcriture.get(i)));
                save.setVectorEcriture(vectorEcriture);
                checkBox.addActionListener(save);
                this.add(checkBox);
                
                JButton details = new JButton();
                details.setText("détails");
                details.reshape(700, y, 80, 20);
                details.addActionListener(new GoJournalListener((Ecriture)listesEcriture.get(i),true));
                this.add(details);
                y+=20;
            }
            
            JButton validerSelectionner = new JButton();
            validerSelectionner.setText(" Valider sélections ");
            validerSelectionner.reshape(570, 50, 150, 30);
            validerSelectionner.addActionListener(new ValidationPlusieursEcrituresListener(vectorEcriture));
            this.add(validerSelectionner);
            
            JButton validerTous = new JButton();
            validerTous.setText(" Valider tous ");
            validerTous.reshape(275, 50, 150, 30);
            this.add(validerTous);
            
            this.add(new JButtonRetour());
            
        } catch ( Exception ex ){
            System.out.println(ex.getMessage());
        }
    }
}
