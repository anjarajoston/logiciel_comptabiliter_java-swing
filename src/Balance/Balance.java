/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Balance;

import Compte.Achat;
import Compte.Banque;
import Compte.Capitaux;
import Compte.Fournisseur;
import Compte.Immobilisation;
import Compte.Stock;
import Compte.Vente;
import java.util.ArrayList;

/**
 *
 * @author User_HP
 */
public class Balance {
    ArrayList<Capitaux> capitaux = new ArrayList<>();
    ArrayList<Immobilisation> immobilisation = new ArrayList<>();
    ArrayList<Stock> stock = new ArrayList<>();
    ArrayList<Fournisseur> fournisseur = new ArrayList<>();
    ArrayList<Banque> banque = new ArrayList<>();
    ArrayList<Achat> achat = new ArrayList<>();
    ArrayList<Vente> vente = new ArrayList<>();

    public ArrayList<Capitaux> getCapitaux() {
        return capitaux;
    }

    public void setCapitaux(ArrayList<Capitaux> capitaux) {
        this.capitaux = capitaux;
    }

    public ArrayList<Immobilisation> getImmobilisation() {
        return immobilisation;
    }

    public void setImmobilisation(ArrayList<Immobilisation> immobilisation) {
        this.immobilisation = immobilisation;
    }

    public ArrayList<Stock> getStock() {
        return stock;
    }

    public void setStock(ArrayList<Stock> stock) {
        this.stock = stock;
    }

    public ArrayList<Fournisseur> getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(ArrayList<Fournisseur> fournisseur) {
        this.fournisseur = fournisseur;
    }

    public ArrayList<Banque> getBanque() {
        return banque;
    }

    public void setBanque(ArrayList<Banque> banque) {
        this.banque = banque;
    }

    public ArrayList<Achat> getAchat() {
        return achat;
    }

    public void setAchat(ArrayList<Achat> achat) {
        this.achat = achat;
    }

    public ArrayList<Vente> getVente() {
        return vente;
    }

    public void setVente(ArrayList<Vente> vente) {
        this.vente = vente;
    }
    
    public double [] getSousTotalCapitaux(Balance balance){
        double [] sousTotalCapitaux = new double[2];
        
        for( int i = 0 ; i < balance.getCapitaux().size() ; i++ ){
            sousTotalCapitaux[0] += balance.getCapitaux().get(i).getDebit();
            sousTotalCapitaux[1] += balance.getCapitaux().get(i).getCredit();
        }
        
        return sousTotalCapitaux;
    }
    
    public double [] getSousTotalImmobilisation(Balance balance){
        double [] sousTotalImmobilisation = new double[2];
        
        for( int i = 0 ; i < balance.getImmobilisation().size() ; i++ ){
            sousTotalImmobilisation[0] += balance.getImmobilisation().get(i).getDebit();
            sousTotalImmobilisation[1] += balance.getImmobilisation().get(i).getCredit();
        }
        
        return sousTotalImmobilisation;
    }
    
    public double [] getSousTotalStock(Balance balance){
        double [] sousTotalStock = new double[2];
        
        for( int i = 0 ; i < balance.getStock().size() ; i++ ){
            sousTotalStock[0] += balance.getStock().get(i).getDebit();
            sousTotalStock[1] += balance.getStock().get(i).getCredit();
        }
        
        return sousTotalStock;
    }
    
    public double [] getSousTotalFournisseur(Balance balance){
        double [] sousTotalFournisseur = new double[2];
        
        for( int i = 0 ; i < balance.getFournisseur().size() ; i++ ){
            sousTotalFournisseur[0] += balance.getFournisseur().get(i).getDebit();
            sousTotalFournisseur[1] += balance.getFournisseur().get(i).getCredit();
        }
        
        return sousTotalFournisseur;
    }
    
    public double [] getSousTotalBanque(Balance balance){
        double [] sousTotalBanque = new double[2];
        
        for( int i = 0 ; i < balance.getBanque().size() ; i++ ){
            sousTotalBanque[0] += balance.getBanque().get(i).getDebit();
            sousTotalBanque[1] += balance.getBanque().get(i).getCredit();
        }
        
        return sousTotalBanque;
    }
    
    public double [] getSousTotalAchat(Balance balance){
        double [] sousTotalAchat = new double[2];
        
        for( int i = 0 ; i < balance.getAchat().size() ; i++ ){
            sousTotalAchat[0] += balance.getAchat().get(i).getDebit();
            sousTotalAchat[1] += balance.getAchat().get(i).getCredit();
        }
        
        return sousTotalAchat;
    }
    
    public double [] getSousTotalVente(Balance balance){
        double [] sousTotalVente = new double[2];
        
        for( int i = 0 ; i < balance.getVente().size() ; i++ ){
            sousTotalVente[0] += balance.getVente().get(i).getDebit();
            sousTotalVente[1] += balance.getVente().get(i).getCredit();
        }
        
        return sousTotalVente;
    }
}
