/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JFrame;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author User_HP
 */
public class JFrameAffichage extends JFrame{
    JPanel panel;

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }
    
    public JFrameAffichage(JPanel panel) throws Exception {
        this.setPanel(panel);
        this.setSize(800,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.getContentPane().add(panel);
        this.setVisible(true);
    }
}
