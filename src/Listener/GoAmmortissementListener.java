/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Comptabiter.Erreur;
import Compte.Immobilisation;
import JFrame.JFrameAffichage;
import JFrame.JFrameErreur;
import JPanel.JPanelErreur;
import JPanel.JPanelTableImmobilisation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author User_HP
 */
public class GoAmmortissementListener implements ActionListener{
    JTextField textFieldLibelle;
    JTextField textFieldMontant;
    JTextField textFieldAmmortissement;
    JTextField date;

    public JTextField getDate() {
        return date;
    }

    public void setDate(JTextField date) {
        this.date = date;
    }

    public JTextField getTextFieldLibelle() {
        return textFieldLibelle;
    }

    public void setTextFieldLibelle(JTextField textFieldLibelle) {
        this.textFieldLibelle = textFieldLibelle;
    }

    public JTextField getTextFieldMontant() {
        return textFieldMontant;
    }

    public void setTextFieldMontant(JTextField textFieldMontant) {
        this.textFieldMontant = textFieldMontant;
    }

    public JTextField getTextFieldAmmortissement() {
        return textFieldAmmortissement;
    }

    public void setTextFieldAmmortissement(JTextField textFieldAmmortissement) {
        this.textFieldAmmortissement = textFieldAmmortissement;
    }
    
    public GoAmmortissementListener(JTextField textFieldLibelle,JTextField textFieldMontant,JTextField textFieldAmmortissement,JTextField date){
        setTextFieldLibelle(textFieldLibelle);
        setTextFieldMontant(textFieldMontant);
        setTextFieldAmmortissement(textFieldAmmortissement);
        setDate(date);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            System.out.println("libelle = "+getTextFieldLibelle().getText()+" | montant = "+getTextFieldMontant().getText()+" | année ammortissement = "+getTextFieldAmmortissement().getText()+" date :"+getDate());
            
            Erreur erreurImmobilisation = new Immobilisation().testInsertImmobilisation(getTextFieldLibelle().getText(),getTextFieldMontant().getText(),getTextFieldAmmortissement().getText(),getDate().getText());
            if( erreurImmobilisation.getEtat() ){
                Immobilisation immo = new Immobilisation();
                immo.setLibelle(getTextFieldLibelle().getText());
                immo.setMontant(Double.parseDouble(getTextFieldMontant().getText()));
                immo.setAnneeAmmortissement(Integer.parseInt(getTextFieldAmmortissement().getText()));
                immo.setDate(getDate().getText());
                
                JTable tableAmmortissement = immo.getTableauAmmortissement(immo);

                JPanelTableImmobilisation panelImmobilisation = new JPanelTableImmobilisation(tableAmmortissement,immo,"");
                JFrameAffichage frame = new JFrameAffichage(panelImmobilisation);
            } else {
                JPanelErreur panelErreur = new JPanelErreur(erreurImmobilisation.getMessage());
                JFrameErreur frameErreur = new JFrameErreur(panelErreur);
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
