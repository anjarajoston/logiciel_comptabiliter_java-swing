/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Comptabiter.Exercice;
import Comptabiter.Journal;
import Comptabiter.Mois;
import JFrame.JFrameAffichage;
import JPanel.JPanelEcriture;
import Mere.Ecriture;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

/**
 *
 * @author User_HP
 */
public class GoJournalListener implements ActionListener{
    JComboBox jour;
    Exercice exercice;
    Mois mois;
    Journal journal;
    Ecriture ecriture;
    boolean isDetail;

    public Ecriture getEcriture() {
        return ecriture;
    }

    public void setEcriture(Ecriture ecriture) {
        this.ecriture = ecriture;
    }

    public boolean getIsDetail() {
        return isDetail;
    }

    public void setIsDetail(boolean isDetail) {
        this.isDetail = isDetail;
    }

    public JComboBox getJour() {
        return jour;
    }

    public void setJour(JComboBox jour) {
        this.jour = jour;
    }

    public Exercice getExercice() {
        return exercice;
    }

    public void setExercice(Exercice exercice) {
        this.exercice = exercice;
    }

    public Mois getMois() {
        return mois;
    }

    public void setMois(Mois mois) {
        this.mois = mois;
    }

    public Journal getJournal() {
        return journal;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }
    public GoJournalListener(JComboBox jour,Exercice exercice,Mois mois,Journal journal){
        setJour(jour);
        setExercice(exercice);
        setMois(mois);
        setJournal(journal);
        setIsDetail(false);
    }
    
    public GoJournalListener(Ecriture ecriture,boolean isDetail){
        setEcriture(ecriture);
        setIsDetail(isDetail);
    }
            
    public void actionPerformed(ActionEvent e){
        try { 
            if( getIsDetail() ){
                JPanelEcriture panelEcriture = new JPanelEcriture(getEcriture());
                JFrameAffichage frame= new JFrameAffichage(panelEcriture);
            } else {
                JPanelEcriture panelEcriture = new JPanelEcriture((Integer)getJour().getItemAt(getJour().getSelectedIndex()),getExercice(),getMois(),getJournal());
                JFrameAffichage frame= new JFrameAffichage(panelEcriture);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
