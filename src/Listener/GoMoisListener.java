/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Comptabiter.Exercice;
import Comptabiter.Mois;
import JFrame.JFrameAffichage;
import JPanel.JPanelJournal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author User_HP
 */
public class GoMoisListener implements ActionListener{
    Exercice exercice;
    Mois mois;

    public Exercice getExercice() {
        return exercice;
    }

    public void setExercice(Exercice exercice) {
        this.exercice = exercice;
    }

    public Mois getMois() {
        return mois;
    }

    public void setMois(Mois mois) {
        this.mois = mois;
    }
    
    public GoMoisListener(Exercice exercice,Mois mois){
        setExercice(exercice);
        setMois(mois);
    }
            
    public void actionPerformed(ActionEvent e){
        try { 
            JPanelJournal panelJournal = new JPanelJournal(getExercice(),getMois());
            JFrameAffichage frame= new JFrameAffichage(panelJournal);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
