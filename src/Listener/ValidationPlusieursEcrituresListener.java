/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Fille.SousEcriture;
import JFrame.JFrameAffichage;
import JFrame.JFrameErreur;
import JPanel.JPanelErreur;
import JPanel.JPanelExercice;
import Mere.Ecriture;
import Validation.Valider;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

/**
 *
 * @author User_HP
 */
public class ValidationPlusieursEcrituresListener implements ActionListener{
    Vector<Ecriture> listesEcritures;

    public Vector<Ecriture> getListesEcritures() {
        return listesEcritures;
    }

    public void setListesEcritures(Vector<Ecriture> listesEcritures) {
        this.listesEcritures = listesEcritures;
    }
    
    public ValidationPlusieursEcrituresListener(Vector<Ecriture> listesEcritures){
        setListesEcritures(listesEcritures);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if( getListesEcritures().isEmpty() ){
                JPanelErreur panelErreur = new JPanelErreur(" Pas de confirmation en vue :( !");
                JFrameErreur frameErreur = new JFrameErreur(panelErreur);
            } else {
                
                for( int i = 0 ; i < getListesEcritures().size() ; i++ ){
                    System.out.println(" id à confirmer "+getListesEcritures().elementAt(i).getId());

                    Ecriture ecriture = getListesEcritures().elementAt(i);

                    Valider valider = new Valider("validerEcriture",ecriture.getId(),1);
                    valider.valider(null);
                    ecriture.setIdObjet(ecriture.getId());
                    ecriture.setEtat(2);
                    System.out.println(ecriture.getRequetUpdate(ecriture));
                    ecriture.update(null);

                    ecriture.setTableFille("sousecriture");
                    ecriture.setLiaison("idecriture");
                    SousEcriture sousEcriture = new SousEcriture();
                    ecriture.setFille(sousEcriture);
                    ecriture.setId(ecriture.getId());
                    ecriture.setIdMere(ecriture.getId());

                    Vector tableauxFilles = ecriture.getTableauFille(null);

                    for( int j = 0 ; j < tableauxFilles.size() ; j++ ){
                        SousEcriture sousEcr = (SousEcriture)tableauxFilles.elementAt(j);
                        sousEcr.setIdObjet(sousEcr.getId());
                        sousEcr.setEtat(2);
                        sousEcr.update(null);
                    }

                }
                JPanelExercice panelExercice = new JPanelExercice();
                JFrameAffichage frame = new JFrameAffichage(panelExercice);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
