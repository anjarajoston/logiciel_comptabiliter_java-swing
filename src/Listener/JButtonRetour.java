/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Comptabiter.Exercice;
import JFrame.JFrameAffichage;
import JPanel.JPanelExercice;
import JPanel.JPanelMois;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author User_HP
 */
public class JButtonRetour extends JButton implements ActionListener{
    boolean etat = false;

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }
    
    public JButtonRetour(){
        this.setText("Retour");
        this.addActionListener(this);
        this.reshape(530, 1, 80, 30);
    }
    
    @Override
     public void actionPerformed(ActionEvent e){
        try {
            if( getEtat() ){
                JPanelExercice panelExercice = new JPanelExercice();
                JFrameAffichage frame= new JFrameAffichage(panelExercice);
            } else {
                this.getFocusCycleRootAncestor().setVisible(false);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
