/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Mere.Ecriture;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

/**
 *
 * @author User_HP
 */
public class SaveEcritureToValideListener implements ActionListener{
    Ecriture ecriture;
    Vector<Ecriture> VectorEcriture;
    int indice;
    boolean etat = false;

    public Ecriture getEcriture() {
        return ecriture;
    }

    public void setEcriture(Ecriture ecriture) {
        this.ecriture = ecriture;
    }

    public Vector<Ecriture> getVectorEcriture() {
        return VectorEcriture;
    }

    public void setVectorEcriture(Vector<Ecriture> VectorEcriture) {
        this.VectorEcriture = VectorEcriture;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public SaveEcritureToValideListener(Ecriture ecriture){
        setEcriture(ecriture);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if( getVectorEcriture().isEmpty() ){
                getVectorEcriture().add(getEcriture());
                System.out.println(" miampy id = "+getEcriture().getId());
            } else {
                for( int i = 0 ; i < getVectorEcriture().size() ; i++ ){
                    if( getVectorEcriture().elementAt(i).getId() == getEcriture().getId() ){
                        setEtat(true);
                        setIndice(i);
                    }
                } 

                if( getEtat() ){
                    System.out.println(" miala id = "+getVectorEcriture().elementAt(getIndice()).getId());
                    getVectorEcriture().remove(getIndice());
                } else {
                    getVectorEcriture().add(getEcriture());
                    System.out.println(" miampy id = "+getEcriture().getId());
                }
            }
            
            System.out.println(" isan'ny ato "+getVectorEcriture().size());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
