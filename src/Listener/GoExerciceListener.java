/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Comptabiter.Exercice;
import JFrame.JFrameAffichage;
import JPanel.JPanelMois;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author User_HP
 */
public class GoExerciceListener implements ActionListener{
    Exercice exercice;

    public Exercice getExercice() {
        return exercice;
    }

    public void setExercice(Exercice exercice) {
        this.exercice = exercice;
    }

    public GoExerciceListener(Exercice exercice){
        setExercice(exercice);
    }
    
    public void actionPerformed(ActionEvent e){
        try { 
            JPanelMois panelMois = new JPanelMois(getExercice());
            JFrameAffichage frame= new JFrameAffichage(panelMois);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
