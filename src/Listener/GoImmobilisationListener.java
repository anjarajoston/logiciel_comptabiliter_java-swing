/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import JFrame.JFrameAffichage;
import JPanel.JPanelImmobilisation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author User_HP
 */
public class GoImmobilisationListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            JPanelImmobilisation panelImmobilisation = new JPanelImmobilisation();
            JFrameAffichage frameAffichage = new JFrameAffichage(panelImmobilisation);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
