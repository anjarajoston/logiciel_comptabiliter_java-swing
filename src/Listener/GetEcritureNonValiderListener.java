/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import JFrame.JFrameAffichage;
import JPanel.JPanelListesEcritures;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author User_HP
 */
public class GetEcritureNonValiderListener implements ActionListener{
    public void actionPerformed(ActionEvent e){
        try {
            JPanelListesEcritures panelListesEcritures = new JPanelListesEcritures();
            JFrameAffichage frame = new JFrameAffichage(panelListesEcritures);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
