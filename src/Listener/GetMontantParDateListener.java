/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Comptabiter.Erreur;
import Compte.Immobilisation;
import JFrame.JFrameAffichage;
import JFrame.JFrameErreur;
import JPanel.JPanelErreur;
import JPanel.JPanelTableImmobilisation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author User_HP
 */
public class GetMontantParDateListener implements ActionListener{
    JTextField textFieldLibelle;
    Immobilisation immo;
    String dateAnio;

    public Immobilisation getImmo() {
        return immo;
    }

    public void setImmo(Immobilisation immo) {
        this.immo = immo;
    }

    public JTextField getTextFieldLibelle() {
        return textFieldLibelle;
    }

    public void setTextFieldLibelle(JTextField textFieldLibelle) {
        this.textFieldLibelle = textFieldLibelle;
    }

    public String getDateAnio() {
        return dateAnio;
    }

    public void setDateAnio(String dateAnio) {
        this.dateAnio = dateAnio;
    }
    
    public GetMontantParDateListener(JTextField textFieldLibelle,Immobilisation immo,String dateAnio){
        setTextFieldLibelle(textFieldLibelle);
        setImmo(immo);
        setDateAnio(dateAnio);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Erreur erreurDateCession = new Immobilisation().testDateCession(getTextFieldLibelle().getText());
            if( erreurDateCession.getEtat() ){
                System.out.println(" date demande "+getTextFieldLibelle().getText());
                System.out.println(" date androany "+getDateAnio());

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date firstDate = sdf.parse(getDateAnio());
                Date secondDate = sdf.parse(getTextFieldLibelle().getText());

                long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
                long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

                System.out.println(" difference : "+diff);

                double montantaDemande = immo.getMontant();
                double pourcentage = montantaDemande/(immo.getAnneeAmmortissement());

                System.out.println(" miala isan'taona : "+pourcentage);

                double montantParJour = pourcentage/365;

                System.out.println(" montant isan'andro : "+montantParJour);

                double montantFinal = (montantaDemande)-(montantParJour*(Double.parseDouble(((Long)diff).toString())));

                System.err.println(" montant amarotana azy :"+montantFinal);

                immo.setEtat(true);
                immo.setMontantVente(montantFinal);

                JTable tableAmmortissement = immo.getTableauAmmortissement(immo);

                JPanelTableImmobilisation panelImmobilisation = new JPanelTableImmobilisation(tableAmmortissement,immo,getTextFieldLibelle().getText());
                JFrameAffichage frame = new JFrameAffichage(panelImmobilisation);
            }
            else {
                JPanelErreur panelErreur = new JPanelErreur(erreurDateCession.getMessage());
                JFrameErreur frameErreur = new JFrameErreur(panelErreur);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
