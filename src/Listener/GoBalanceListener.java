/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Balance.Balance;
import Compte.Achat;
import Compte.Banque;
import Compte.Capitaux;
import Compte.Fournisseur;
import Compte.Immobilisation;
import Compte.Stock;
import Compte.Vente;
import Fille.SousEcriture;
import JFrame.JFrameAffichage;
import JPanel.JPanelBalance;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JTable;

/**
 *
 * @author User_HP
 */
public class GoBalanceListener implements ActionListener{
    public void actionPerformed(ActionEvent e){
        try{
            Balance balance = new Balance();
            
            SousEcriture sousEcriture = new SousEcriture();
            sousEcriture.setEtat(2);
            System.out.println(sousEcriture.getRequetFind(sousEcriture));
            ArrayList<Object> listesSousEcriture = sousEcriture.find(null);
            for( int i = 0 ; i < listesSousEcriture.size() ; i++ ){
                //System.out.println(((SousEcriture)listesSousEcriture.get(i)).getCompte());
                switch (((SousEcriture)listesSousEcriture.get(i)).getCompte()) {
                    case 1:
                        Capitaux capitaux = new Capitaux();
                        capitaux.setIntitule(((SousEcriture)listesSousEcriture.get(i)).getIntitule());
                        capitaux.setDebit(((SousEcriture)listesSousEcriture.get(i)).getDebit());
                        capitaux.setCredit(((SousEcriture)listesSousEcriture.get(i)).getCredit());
                        balance.getCapitaux().add(capitaux);
                        //System.out.println("capitaux zan!");
                        break;
                    case 2:
                        Immobilisation immobilisation = new Immobilisation();
                        immobilisation.setIntitule(((SousEcriture)listesSousEcriture.get(i)).getIntitule());
                        immobilisation.setDebit(((SousEcriture)listesSousEcriture.get(i)).getDebit());
                        immobilisation.setDebit(((SousEcriture)listesSousEcriture.get(i)).getCredit());
                        balance.getImmobilisation().add(immobilisation);
                        //System.out.println("immo zan!");
                        break;
                    case 3:
                        Stock stock = new Stock();
                        stock.setIntitule(((SousEcriture)listesSousEcriture.get(i)).getIntitule());
                        stock.setDebit(((SousEcriture)listesSousEcriture.get(i)).getDebit());
                        stock.setCredit(((SousEcriture)listesSousEcriture.get(i)).getCredit());
                        balance.getStock().add(stock);
                        //System.out.println("stock zan!");
                        break;
                    case 4:
                        Fournisseur fournisseur = new Fournisseur();
                        fournisseur.setIntitule(((SousEcriture)listesSousEcriture.get(i)).getIntitule());
                        fournisseur.setDebit(((SousEcriture)listesSousEcriture.get(i)).getDebit());
                        fournisseur.setCredit(((SousEcriture)listesSousEcriture.get(i)).getCredit());
                        balance.getFournisseur().add(fournisseur);
                        //System.out.println("fournisseur zan!");
                        break;
                    case 5:
                        Banque banque = new Banque();
                        banque.setIntitule(((SousEcriture)listesSousEcriture.get(i)).getIntitule());
                        banque.setDebit(((SousEcriture)listesSousEcriture.get(i)).getDebit());
                        banque.setCredit(((SousEcriture)listesSousEcriture.get(i)).getCredit());
                        balance.getBanque().add(banque);
                        //System.out.println("banque zan!");
                        break;
                    case 6:
                        Achat achat = new Achat();
                        achat.setIntitule(((SousEcriture)listesSousEcriture.get(i)).getIntitule());
                        achat.setDebit(((SousEcriture)listesSousEcriture.get(i)).getDebit());
                        achat.setCredit(((SousEcriture)listesSousEcriture.get(i)).getCredit());
                        balance.getAchat().add(achat);
                        //System.out.println("achat zan!");
                        break;
                    case 7:
                        Vente vente = new Vente();
                        vente.setIntitule(((SousEcriture)listesSousEcriture.get(i)).getIntitule());
                        vente.setDebit(((SousEcriture)listesSousEcriture.get(i)).getDebit());
                        vente.setCredit(((SousEcriture)listesSousEcriture.get(i)).getCredit());
                        balance.getVente().add(vente);
                        //System.out.println("vente zan!");
                        break;
                    default:
                        break;
                }
            }
            
            String [] enTete = new String[6];
            enTete[0] = "Nom compte";enTete[1] = "Numero compte";enTete[2] = "Débit";enTete[3] = "Crédit";enTete[4] = "Solde débiteur";enTete[5] = "Solde créditeur";
            
            JTable tableCapitaux = new Capitaux().getTableBalanceCompte(new Capitaux(), "1", balance, enTete);
            
            JTable tableImmobilisation = new Immobilisation().getTableBalanceCompte(new Immobilisation(), "2", balance, enTete);
            
            JTable tableStock = new Stock().getTableBalanceCompte(new Stock(), "3", balance, enTete);
            
            JTable tableFournisseur = new Fournisseur().getTableBalanceCompte(new Fournisseur(), "4", balance, enTete);
                    
            JTable tableBanque = new Banque().getTableBalanceCompte(new Banque(), "5", balance, enTete);
            
            JTable tableAchat = new Achat().getTableBalanceCompte(new Achat(), "6", balance, enTete);
            
            JTable tableVente = new Vente().getTableBalanceCompte(new Vente(), "7", balance, enTete);
            
            JPanelBalance panelMois = new JPanelBalance(tableCapitaux,tableImmobilisation,tableStock,tableFournisseur,tableBanque,tableAchat,tableVente);
            JFrameAffichage frame= new JFrameAffichage(panelMois);
        } catch( Exception ex ) {
            ex.printStackTrace();
        }
    }
}
