/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Comptabiter.Compte;
import Comptabiter.Erreur;
import Fille.SousEcriture;
import JFrame.JFrameAffichage;
import JFrame.JFrameErreur;
import JPanel.JPanelEcriture;
import JPanel.JPanelErreur;
import Mere.Ecriture;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 *
 * @author User_HP
 */
public class GoSauvegarde implements ActionListener{
    Ecriture ecriture;
    JComboBox compte;
    JTextField reference;
    JTextField debit;
    JTextField credit;
    Vector<SousEcriture> vector = new Vector<SousEcriture>();
    JPanelEcriture panelEcriture;

    public Ecriture getEcriture() {
        return ecriture;
    }

    public void setEcriture(Ecriture ecriture) {
        this.ecriture = ecriture;
    }

    public JComboBox getCompte() {
        return compte;
    }

    public void setCompte(JComboBox compte) {
        this.compte = compte;
    }

    public JTextField getReference() {
        return reference;
    }

    public void setReference(JTextField reference) {
        this.reference = reference;
    }

    public JTextField getDebit() {
        return debit;
    }

    public void setDebit(JTextField debit) {
        this.debit = debit;
    }

    public JTextField getCredit() {
        return credit;
    }

    public void setCredit(JTextField credit) {
        this.credit = credit;
    }

    public Vector<SousEcriture> getVector() {
        return vector;
    }

    public void setVector(Vector<SousEcriture> vector) {
        this.vector = vector;
    }

    public JPanelEcriture getPanelEcriture() {
        return panelEcriture;
    }

    public void setPanelEcriture(JPanelEcriture panelEcriture) {
        this.panelEcriture = panelEcriture;
    }
    
    public GoSauvegarde(Ecriture ecriture,JComboBox compte,JTextField reference,JTextField debit,JTextField credit,JPanelEcriture panelEcriture){
        setEcriture(ecriture);
        setCompte(compte);
        setReference(reference);
        setDebit(debit);
        setCredit(credit);
        //setPanelEcriture(panelEcriture);
        //panelEcriture.revalidate();
        panelEcriture.repaint();
    }
    
    public void actionPerformed(ActionEvent e){
        try { 
            /*Ecriture ecriture = new Ecriture((Integer)getComboBox().getItemAt(getComboBox().getSelectedIndex()),getDebit().getText(),getCredit().getText());
            getVector().add(ecriture);
            for (int i = 0 ; i < getVector().size() ; i++ ){
                System.out.println(" compte : "+getVector().get(i).getCompte());
                System.out.println(" debit : "+getVector().get(i).getDebit());
                System.out.println(" credit : "+getVector().get(i).getCredit());
                System.out.println("------------------------------"+i);
            }*/
            Connection c = null;
            SousEcriture sousEcriture = new SousEcriture();
            /*-------------------- A propos Ecriture --------------------*/
            Ecriture ecriture = getEcriture();
            ArrayList<Object> listeEcriture = ecriture.find(c);
            if( listeEcriture.size() == 0 ){
                System.out.println("mbola tsy misy ilay exercice");
                System.out.println("req="+ecriture.getRequetInsert(ecriture));
                ecriture.create(c);
                ArrayList<Object> newListeEcriture = ecriture.find(c);
                ecriture.setId(((Ecriture)newListeEcriture.get(0)).getId());
            } else {
                System.out.println("efa misy");
                ecriture.setId(((Ecriture)listeEcriture.get(0)).getId());
            }

            /*-----------------------------------------------------------*/
            /*-------------------- A propos Compte --------------------*/
            Compte compte = new Compte();
            compte.setCompte((Integer)getCompte().getItemAt(getCompte().getSelectedIndex()));
            ArrayList<Object> listeCompte = compte.find(c);
            compte.setId(((Compte)listeCompte.get(0)).getId());
            compte.setIntitule(((Compte)listeCompte.get(0)).getIntitule());

            /*----------------------------- TEST Sous-Ecriture ------------------------------*/
            Erreur erruerSousEcriture = sousEcriture.testInsertSousEcriture(getCompte().getItemAt(getCompte().getSelectedIndex()).toString(), getDebit().getText(), getCredit().getText());
            if( erruerSousEcriture.getEtat() ){
                String numeroCompte = getCompte().getItemAt(getCompte().getSelectedIndex()).toString().substring(0, 1);
                sousEcriture.setIdEcriture(ecriture.getId());
                sousEcriture.setIdcompte(compte.getId());
                sousEcriture.setIntitule(compte.getCompte());
                sousEcriture.setLibelle(compte.getIntitule());
                sousEcriture.setReference(getReference().getText());
                sousEcriture.setDebit(Double.parseDouble(getDebit().getText()));
                sousEcriture.setCredit(Double.parseDouble(getCredit().getText()));
                sousEcriture.setCompte(Integer.parseInt(numeroCompte));
                
                System.out.println("idEcriture"+sousEcriture.getIdEcriture()+"|idCompte="+sousEcriture.getIdcompte()+"|intitule="+sousEcriture.getIntitule()+"|libelle="+sousEcriture.getLibelle()+"|reference="+sousEcriture.getReference()+"|Debit="+sousEcriture.getDebit()+"|Credit="+sousEcriture.getCredit());
                System.out.println("req="+sousEcriture.getRequetInsert(sousEcriture));
                sousEcriture.create(c);
                getVector().add(sousEcriture);
                
                JPanelEcriture panelEcriture = new JPanelEcriture(ecriture);
                JFrameAffichage frame = new JFrameAffichage(panelEcriture);
            } else {
                JPanelErreur panelErreur = new JPanelErreur(erruerSousEcriture.getMessage());
                JFrameErreur frameErreur = new JFrameErreur(panelErreur);
            }
            
            /*------------------------------------------------------------------------------*/
            
            
            getPanelEcriture().revalidate();
            getPanelEcriture().repaint();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
