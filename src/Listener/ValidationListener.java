/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import Comptabiter.Erreur;
import Fille.SousEcriture;
import JFrame.JFrameAffichage;
import JFrame.JFrameErreur;
import JPanel.JPanelErreur;
import JPanel.JPanelExercice;
import Mere.Ecriture;
import Validation.Valider;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;
import javax.swing.JPanel;

/**
 *
 * @author User_HP
 */
public class ValidationListener implements ActionListener{
    Ecriture ecriture;
    JPanel panel;

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public Ecriture getEcriture() {
        return ecriture;
    }

    public void setEcriture(Ecriture ecriture) {
        this.ecriture = ecriture;
    }
    
    public ValidationListener(Ecriture ecriture,JPanel panel){
        setEcriture(ecriture);
        setPanel(panel);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        try { 
            try {
                Erreur erreur = getEcriture().testValidationEcriture(getEcriture());
                
                Valider valider = new Valider("validerEcriture",getEcriture().getId(),1);
                valider.valider(null);
                getEcriture().setIdObjet(getEcriture().getId());
                getEcriture().setEtat(2);
                System.out.println(getEcriture().getRequetUpdate(getEcriture()));
                getEcriture().update(null);
                
                getEcriture().setTableFille("sousecriture");
                getEcriture().setLiaison("idecriture");
                SousEcriture sousEcriture = new SousEcriture();
                getEcriture().setFille(sousEcriture);
                getEcriture().setId(getEcriture().getId());
                getEcriture().setIdMere(getEcriture().getId());
                
                Vector tableauxFilles = getEcriture().getTableauFille(null);
                
                for( int i = 0 ; i < tableauxFilles.size() ; i++ ){
                    SousEcriture sousEcr = (SousEcriture)tableauxFilles.elementAt(i);
                    sousEcr.setIdObjet(sousEcr.getId());
                    sousEcr.setEtat(2);
                    sousEcr.update(null);
                }
                
                JPanelExercice panelExercice = new JPanelExercice();
                JFrameAffichage frame = new JFrameAffichage(panelExercice);
            } catch (Exception ex) {
                JPanelErreur panelErreur = new JPanelErreur(ex.getMessage());
                JFrameErreur frameErreur = new JFrameErreur(panelErreur);
                ((JFrame.JFrameAffichage)getPanel().getFocusCycleRootAncestor()).setDefaultCloseOperation(0);
                
                ((JFrame.JFrameAffichage)getPanel().getFocusCycleRootAncestor()).addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent ev){
                        new JFrameErreur(new JPanelErreur(ex.getMessage()));
                    }  
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
