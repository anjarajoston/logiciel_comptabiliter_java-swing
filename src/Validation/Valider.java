/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validation;

import Connection.ConnectionDB;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;

/**
 *
 * @author User_HP
 */
public class Valider {
    String nomTable;
    int idObject;
    int idUser;

    public String getNomTable() {
        return nomTable;
    }

    public void setNomTable(String nomTable) {
        this.nomTable = nomTable;
    }

    public int getIdObject() {
        return idObject;
    }

    public void setIdObject(int idObject) {
        this.idObject = idObject;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
    
    public Valider(String nomTable,int idObject,int idUser){
        setNomTable(nomTable);
        setIdObject(idObject);
        setIdUser(idUser);
    }
     
    public String getNomColonne(String nomTable){
        char [] splitSql = nomTable.toCharArray();
        return "id"+nomTable.substring(7, splitSql.length);
    } 
     
    public void valider(Connection c) throws Exception{
        Connection connection = c;
        if(connection == null){
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
        } 
        PreparedStatement statement = null;
        ResultSet result = null;
        try{
            String requetSql = "insert into "+getNomTable()+"("+getNomColonne(getNomTable())+",idUser,dateAcceptation) values('"+getIdObject()+"','"+getIdUser()+"','"+Date.valueOf(LocalDate.now()).toString()+"')";
            System.out.println(requetSql);
            statement = connection.prepareStatement(requetSql);
            statement.executeUpdate();
        } catch(Exception e){
            throw e;
        } finally{
            if(result != null){
                result.close();
            }
            if(statement != null){
                statement.close();
            }
            if(connection != null){
                connection.close();
            }
        }
     }
}
