/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objet;

import Balance.Balance;
import Compte.Achat;
import Compte.Banque;
import Compte.Capitaux;
import Compte.Fournisseur;
import Compte.Immobilisation;
import Compte.Stock;
import Compte.Vente;
import Connection.ConnectionDB;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author User_HP
 */
public class ObjetBdd {
    int idObjet;
    String [] enTete;

    public int getIdObjet() {
        return idObjet;
    }

    public void setIdObjet(int idObjet) {
        this.idObjet = idObjet;
    }

    public String[] getEnTete() {
        return enTete;
    }

    public void setEnTete(String[] enTete) {
        this.enTete = enTete;
    }
    
/*================================= maka liste attribut class(nom,prenom,...) =============================================*/
   public String [] getAttributObject(Object obj){
        Field [] attribut = obj.getClass().getDeclaredFields();
        String [] attr = new String[attribut.length];
        for( int  i = 0 ; i < attribut.length ; i++ ){
            attr[i] = attribut[i].getName();
        }
        return attr;
    }
/*================================= maka liste type attribut class(int,String,...) =============================================*/
   public Class [] getTypeAttributObject(Object obj){
        Field [] attribut = obj.getClass().getDeclaredFields();
        Class [] clas = new Class[attribut.length];
        for( int  i = 0 ; i < attribut.length ; i++ ){
            clas[i] = attribut[i].getType();
        }
        return clas;
    }
/*================================= mamadika attribut en Majuscule 1er lettre pour get and set =================================*/
    public String [] enMajuscule(String [] attr){
        String [] attrTransfo = new String[attr.length];
        for( int  i = 0 ; i < attr.length ; i++ ){
            char [] spliter = attr[i].toCharArray();
            char unMaj = Character.toUpperCase(spliter[0]);
            spliter[0] = unMaj;
            attrTransfo[i] = String.copyValueOf(spliter);
        }
        return attrTransfo;
    }
/*================================= maka anarany class =============================================*/
    public String getNomClasse(Object obj){
        return obj.getClass().getSimpleName();
    }
/*================================= maka ireo fonctions get na set rehetra ao @ilay objet =================================*/
    public String [] getGetAndSet(String getSet,Object obj) {
        String [] attribut = this.getAttributObject(obj);
        String [] tabGetAndSet = new String[attribut.length];
        for ( int i = 0 ; i < attribut.length ; i++ ){
            tabGetAndSet[i] = getSet+(this.enMajuscule(attribut))[i];
        }
        return tabGetAndSet;
    }
/*================================= teste contenu an'ilay objet =================================*/
    public boolean testElement(Object obj){
        if( obj instanceof Integer || obj instanceof Double || obj instanceof Long ){
            if( obj instanceof Integer && ((Integer)obj).intValue() == 0 ){
                return false;
            } else if(obj instanceof Double && ((Double)obj).doubleValue() == 0.0 ){
                return false;
            } else if(obj instanceof Float && ((Float)obj).floatValue() == 0.0 ){
                return false;
            } else if(obj instanceof Long && ((Long)obj).longValue() == 0.0 ){
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
/*================================= maka getters object =================================*/
    public Method [] getValeur(Object obj) throws NoSuchMethodException{
        String [] getAndSet = this.getGetAndSet("get", obj);
        Method [] method = new Method[getAndSet.length];
        for ( int i = 0 ; i < getAndSet.length ; i++ ){
            method[i] = obj.getClass().getDeclaredMethod(getAndSet[i]);
        }
        return method;
    }
    /*************************** Par rapport en-tete ******************************/
    public Method [] getValeurEnTete(ObjetBdd obj) throws NoSuchMethodException{
        String [] enTete = obj.getEnTete();
        Method [] method = new Method[enTete.length];
        for ( int i = 0 ; i < enTete.length ; i++ ){
            method[i] = obj.getClass().getDeclaredMethod("get"+this.enMajuscule(enTete)[i]);
        }
        return method;
    }
    /*********************************************************/
/*================================= construire requette insert =================================*/
    public String getRequetInsert(Object obj) throws Exception{
        String nomTable = this.getNomClasse(obj);
        String sql = "insert into "+nomTable;
        String colonne = "(";
        String valeur = "(";
        String [] attribut = this.getAttributObject(obj);
        Method [] method = this.getValeur(obj);
        int i = 0;
        while( i < method.length ){
            try {
                if( i == method.length - 1 ){
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            colonne += attribut[i];
                            valeur += "'"+method[i].invoke(obj).toString()+"'";
                            break;
                        } else {
                            char [] splitCol = colonne.toCharArray();
                            colonne = colonne.substring(0, splitCol.length-1);

                            char [] splitVal = valeur.toCharArray();
                            valeur = valeur.substring(0, splitVal.length-1);
                            break;
                        }
                    } else {
                        char [] splitCol = colonne.toCharArray();
                        colonne = colonne.substring(0, splitCol.length-1);
                        
                        char [] splitVal = valeur.toCharArray();
                        valeur = valeur.substring(0, splitVal.length-1);
                        break;
                    }
                } else {
                    if( method[i].invoke(obj) != null ){
                        if(this.testElement(method[i].invoke(obj))){
                            colonne += attribut[i]+",";
                            valeur += "'"+method[i].invoke(obj).toString()+"',";
                            i++;
                        } else {
                            //System.out.println(method[i].invoke(obj)+" 1");
                            i++;
                        }
                    } else {
                        //System.out.println(method[i].invoke(obj)+" 2");
                        i++;
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }
        }
        colonne += ")";
        valeur += ")";
        sql += colonne+" values"+valeur;
        return sql;
    }
 /*================================= create fonction =================================*/   
    public void create(Connection c) throws Exception{
        String requetSql = this.getRequetInsert(this);
        Connection connection = c;
        if(connection == null){
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
        } 
        PreparedStatement statement = null;
        ResultSet result = null;
        try{
            statement = connection.prepareStatement(requetSql);
            statement.executeUpdate();
        } catch(Exception e){
            throw e;
        } finally{
            if(result != null){
                result.close();
            }
            if(statement != null){
                statement.close();
            }
            if(connection != null){
                connection.close();
            }
        }
    }
/*================================= find fonction(fille) =================================*/   
    public Vector findFille(ResultSet result) throws Exception{
        Vector listesThis = new Vector();
        String [] attribut = this.getAttributObject(this);
        Class [] typeAttribut = this.getTypeAttributObject(this);
        String [] settersObject = this.getGetAndSet("set", this);
        try{
            while(result.next()){
                Object obj = new Object();
                obj = this.getClass().getDeclaredConstructor().newInstance();
                for ( int j = 0 ; j < settersObject.length ; j++ ){
                    Method method = obj.getClass().getMethod(settersObject[j],typeAttribut[j]);
                    if( typeAttribut[j].isAssignableFrom(String.class) ){
                        method.invoke(obj,result.getString(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(int.class) ){
                        method.invoke(obj,result.getInt(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(double.class) ){
                        method.invoke(obj,result.getDouble(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(float.class) ){
                        method.invoke(obj,result.getFloat(attribut[j]));
                    } 
                    if( typeAttribut[j].isAssignableFrom(long.class) ){
                        method.invoke(obj,result.getLong(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(Date.class) ){
                        method.invoke(obj,result.getDate(attribut[j]));
                    }
                }
                listesThis.add(obj);
            }
        } catch(Exception e){
            throw e;
        } finally{
            if(result != null){
                result.close();
            }
        }
        return listesThis;
    }
/*================================= construire requette find(select) =================================*/  
    public String getRequetFind(Object obj)throws Exception{
        String nomTable = this.getNomClasse(obj);
        String sql = "select ";
        String [] attribut = this.getAttributObject(obj);
        for( int i = 0 ; i < attribut.length ; i++){
            if( i == attribut.length - 1 ){
                sql += attribut[i];
            } else {
                sql += attribut[i]+",";
            }
        }
        sql += " from "+nomTable+" where ";
        Method [] method = this.getValeur(obj);
        int i = 0;
        int j = 0;
        while( i < method.length ){
            try{
                if( i == method.length - 1 ){
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            sql += attribut[i]+" = '"+method[i].invoke(obj).toString()+"'";
                            j++;
                            break;
                        } else {
                            char [] splitSql = sql.toCharArray();
                            sql = sql.substring(0, splitSql.length-4);
                            break;
                        }
                    } else {
                        char [] splitSql = sql.toCharArray();
                        sql = sql.substring(0, splitSql.length-4);
                       break;
                    }
                } else {
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            sql += attribut[i]+" = '"+method[i].invoke(obj).toString()+"' and ";
                            j++;
                            i++;
                        } else {
                            i++;
                        }
                    } else {
                       i++;
                    }
                }
            } catch(Exception ex){
                throw ex;
            }
        }
        if( j == 0 ){
            char [] splitSql = sql.toCharArray();
            sql = sql.substring(0, splitSql.length-3);
        }
        return sql;
    }
 /*================================= find fonction =================================*/   
    public ArrayList<Object> find(Connection c) throws Exception{
        Connection connection = c;
        if(connection == null){
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
        } 
        ArrayList<Object> listesThis = new ArrayList<>();
        String [] attribut = this.getAttributObject(this);
        Class [] typeAttribut = this.getTypeAttributObject(this);
        String [] settersObject = this.getGetAndSet("set", this);
        String requetSql = this.getRequetFind(this);
        PreparedStatement statement = null;
        ResultSet result = null;
        try{
            statement = connection.prepareStatement(requetSql);
            result = statement.executeQuery();
            int indice = 1;
            while(result.next()){
                Object obj = new Object();
                obj = this.getClass().getDeclaredConstructor().newInstance();
                for ( int j = 0 ; j < settersObject.length ; j++ ){
                    Method method = obj.getClass().getMethod(settersObject[j],typeAttribut[j]);
                    if( typeAttribut[j].isAssignableFrom(String.class) ){
                        method.invoke(obj,result.getString(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(int.class) ){
                        method.invoke(obj,result.getInt(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(double.class) ){
                        method.invoke(obj,result.getDouble(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(float.class) ){
                        method.invoke(obj,result.getFloat(attribut[j]));
                    } 
                    if( typeAttribut[j].isAssignableFrom(long.class) ){
                        method.invoke(obj,result.getLong(attribut[j]));
                    }
                    if( typeAttribut[j].isAssignableFrom(Date.class) ){
                        method.invoke(obj,result.getDate(attribut[j]));
                    }
                }
                    listesThis.add(obj);
            }
        } catch(Exception e){
            throw e;
        } finally{
            if(result != null){
                result.close();
            }
            if(statement != null){
                statement.close();
            }
            if(connection != null){
                connection.close();
            }
        }
        return listesThis;
    }
/*================================= construire requette update =================================*/  
    public String getRequetUpdate(Object obj)throws Exception{
        String nomTable = this.getNomClasse(obj);
        String [] attribut = this.getAttributObject(obj);
        String sql = "update "+nomTable+" set ";
        Method [] method = this.getValeur(obj);
        int i = 0;
        while( i < method.length ){
            try{
                if( i == method.length - 1 ){
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            sql += attribut[i]+" = '"+method[i].invoke(obj).toString()+"'";
                            break;
                        } else {
                            char [] splitSql = sql.toCharArray();
                            sql = sql.substring(0, splitSql.length-1);
                            break;
                        }
                    } else {
                        char [] splitSql = sql.toCharArray();
                        sql = sql.substring(0, splitSql.length-1);
                       break;
                    }
                } else {
                    if (method[i].invoke(obj) != null ) {
                        if(this.testElement(method[i].invoke(obj))){
                            sql += attribut[i]+" = '"+method[i].invoke(obj).toString()+"',";
                            i++;
                        } else {
                            i++;
                        }
                    } else {
                       i++;
                    }
                }
            } catch(Exception ex){
                throw ex;
            }
        }
        sql += " where id = "+this.getIdObjet();
        return sql;
    }
/*================================= update fonction =================================*/
    public void update(Connection c) throws Exception{
        String requetSql = this.getRequetUpdate(this);
        Connection connection = c;
        if(connection == null){
            ConnectionDB con = new ConnectionDB();
            connection = con.getConnection("Postgres");
        } 
        PreparedStatement statement = null;
        ResultSet result = null;
        try{
            statement = connection.prepareStatement(requetSql);
            statement.executeUpdate();
        } catch(Exception e){
            throw e;
        } finally{
            if(result != null){
                result.close();
            }
            if(statement != null){
                statement.close();
            }
            if(connection != null){
                connection.close();
            }
        }
    }
    
    public JTable lister(ObjetBdd obj) throws Exception {
        try{
            JTable table = new JTable();

            String [] enTete = obj.getEnTete();
            //System.out.println(obj.getRequetFind(obj));
            ArrayList<Object> liste = obj.find(null);

            Method [] getValeurEnTete = this.getValeurEnTete(obj);

            String [][] valeur = new String [liste.size()][enTete.length];
            
            for( int i = 0 ; i < liste.size() ; i++ ){
                for( int j = 0 ; j < enTete.length ; j++ ){
                    valeur[i][j] = getValeurEnTete[j].invoke(liste.get(i)).toString();
                    //System.out.println(valeur[i][j]);
                    
                }
            }

            DefaultTableModel model = new DefaultTableModel(valeur,enTete);
            table.setModel(model);
            return table;
        } catch ( Exception ex ) {
            throw ex;
        }
    }
    
    public JTable getTableBalanceCompte(ObjetBdd nomCompte,String num,Balance balance,String [] enTete) throws Exception {
        try {
            String numCompte = "compte "+num;
            JTable table = new JTable();
            
            String [][] valeur;
            if( ((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).isEmpty() ){
                valeur = new String[2][enTete.length];
                valeur[0][0] = nomCompte.getClass().getSimpleName();
                valeur[0][1] = "";
                valeur[0][2] = "0.0";
                valeur[0][3] = "0.0";

                valeur[1][0] = "Sous-total "+nomCompte.getClass().getSimpleName();
                valeur[1][1] = numCompte;
                valeur[1][2] = "0.0";
                valeur[1][3] = "0.0";
            } else {
                valeur = new String[((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size()+1][enTete.length];
                for( int i = 0 ; i < ((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size() ; i++ ){
                   if( ((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i) instanceof Capitaux ){
                       valeur[i][0] = nomCompte.getClass().getSimpleName();
                       valeur[i][1] = ((Integer)(((Capitaux)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getIntitule())).toString();
                       valeur[i][2] = ((Capitaux)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit().toString();
                       valeur[i][3] = ((Capitaux)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit().toString();
                   
                       double DebitCreditParSousEcriture = ((Capitaux)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit()-((Capitaux)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit();
                   
                       if( DebitCreditParSousEcriture > 0.0 ){
                           valeur[i][4] = ((Double)DebitCreditParSousEcriture).toString();
                           valeur[i][5] = "";
                       } else {
                           valeur[i][4] = "";
                           valeur[i][5] = ((Double)(DebitCreditParSousEcriture*(-1))).toString();
                       }
                   }
                   else if( ((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i) instanceof Immobilisation ){
                       valeur[i][0] = nomCompte.getClass().getSimpleName();
                       valeur[i][1] = ((Integer)(((Immobilisation)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getIntitule())).toString();
                       valeur[i][2] = ((Immobilisation)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit().toString();
                       valeur[i][3] = ((Immobilisation)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit().toString();
                   
                       double DebitCreditParSousEcriture = ((Immobilisation)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit()-((Immobilisation)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit();
                   
                       if( DebitCreditParSousEcriture > 0.0 ){
                           valeur[i][4] = ((Double)DebitCreditParSousEcriture).toString();
                           valeur[i][5] = "";
                       } else {
                           valeur[i][4] = "";
                           valeur[i][5] = ((Double)(DebitCreditParSousEcriture*(-1))).toString();
                       }
                   }
                   else if( ((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i) instanceof Stock ){
                       valeur[i][0] = nomCompte.getClass().getSimpleName();
                       valeur[i][1] = ((Integer)(((Stock)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getIntitule())).toString();
                       valeur[i][2] = ((Stock)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit().toString();
                       valeur[i][3] = ((Stock)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit().toString();
                   
                       double DebitCreditParSousEcriture = ((Stock)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit()-((Stock)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit();
                   
                       if( DebitCreditParSousEcriture > 0.0 ){
                           valeur[i][4] = ((Double)DebitCreditParSousEcriture).toString();
                           valeur[i][5] = "";
                       } else {
                           valeur[i][4] = "";
                           valeur[i][5] = ((Double)(DebitCreditParSousEcriture*(-1))).toString();
                       }
                   }
                   else if( ((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i) instanceof Fournisseur ){
                       valeur[i][0] = nomCompte.getClass().getSimpleName();
                       valeur[i][1] = ((Integer)(((Fournisseur)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getIntitule())).toString();
                       valeur[i][2] = ((Fournisseur)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit().toString();
                       valeur[i][3] = ((Fournisseur)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit().toString();
                   
                       double DebitCreditParSousEcriture = ((Fournisseur)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit()-((Fournisseur)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit();
                   
                       if( DebitCreditParSousEcriture > 0.0 ){
                           valeur[i][4] = ((Double)DebitCreditParSousEcriture).toString();
                           valeur[i][5] = "";
                       } else {
                           valeur[i][4] = "";
                           valeur[i][5] = ((Double)(DebitCreditParSousEcriture*(-1))).toString();
                       }
                   }
                   else if( ((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i) instanceof Banque ){
                       valeur[i][0] = nomCompte.getClass().getSimpleName();
                       valeur[i][1] = ((Integer)(((Banque)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getIntitule())).toString();
                       valeur[i][2] = ((Banque)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit().toString();
                       valeur[i][3] = ((Banque)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit().toString();
                   
                       double DebitCreditParSousEcriture = ((Banque)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit()-((Banque)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit();
                   
                       if( DebitCreditParSousEcriture > 0.0 ){
                           valeur[i][4] = ((Double)DebitCreditParSousEcriture).toString();
                           valeur[i][5] = "";
                       } else {
                           valeur[i][4] = "";
                           valeur[i][5] = ((Double)(DebitCreditParSousEcriture*(-1))).toString();
                       }
                   }
                   else if( ((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i) instanceof Achat ){
                       valeur[i][0] = nomCompte.getClass().getSimpleName();
                       valeur[i][1] = ((Integer)(((Achat)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getIntitule())).toString();
                       valeur[i][2] = ((Achat)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit().toString();
                       valeur[i][3] = ((Achat)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit().toString();
                   
                       double DebitCreditParSousEcriture = ((Achat)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit()-((Achat)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit();
                   
                       if( DebitCreditParSousEcriture > 0.0 ){
                           valeur[i][4] = ((Double)DebitCreditParSousEcriture).toString();
                           valeur[i][5] = "";
                       } else {
                           valeur[i][4] = "";
                           valeur[i][5] = ((Double)(DebitCreditParSousEcriture*(-1))).toString();
                       }
                   }
                   else if( ((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i) instanceof Vente ){
                       valeur[i][0] = nomCompte.getClass().getSimpleName();
                       valeur[i][1] = ((Integer)(((Vente)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getIntitule())).toString();
                       valeur[i][2] = ((Vente)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit().toString();
                       valeur[i][3] = ((Vente)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit().toString();
                       
                       double DebitCreditParSousEcriture = ((Vente)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getDebit()-((Vente)((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).get(i)).getCredit();
                   
                       if( DebitCreditParSousEcriture > 0.0 ){
                           valeur[i][4] = ((Double)DebitCreditParSousEcriture).toString();
                           valeur[i][5] = "";
                       } else {
                           valeur[i][4] = "";
                           valeur[i][5] = ((Double)(DebitCreditParSousEcriture*(-1))).toString();
                       }
                   }
                }

                double [] sousTotal = (double [])balance.getClass().getDeclaredMethod("getSousTotal"+nomCompte.getClass().getSimpleName(),balance.getClass()).invoke(balance, balance);

                valeur[((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size()][0] = "Sous-total "+nomCompte.getClass().getSimpleName();
                valeur[((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size()][1] = numCompte;
                valeur[((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size()][2] = ((Double)sousTotal[0]).toString();
                valeur[((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size()][3] = ((Double)sousTotal[1]).toString();
            
                double DebitCreditParCompte = sousTotal[0]-sousTotal[1];
                
                if( DebitCreditParCompte > 0.0 ){
                    valeur[((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size()][4] = ((Double)DebitCreditParCompte).toString();
                    valeur[((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size()][5] = "";
                } else {
                    valeur[((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size()][4] = "";
                    valeur[((ArrayList<Object>)balance.getClass().getDeclaredMethod("get"+nomCompte.getClass().getSimpleName()).invoke(balance)).size()][5] = ((Double)(DebitCreditParCompte*(-1))).toString();
                }
                
            }


            DefaultTableModel model = new DefaultTableModel(valeur,enTete);
            table.setModel(model);
            
            return table;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
