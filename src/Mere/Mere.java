/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mere;

import Connection.ConnectionDB;
import Fille.Fille;
import Objet.ObjetBdd;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author User_HP
 */
public class Mere extends ObjetBdd{
     int idMere;
     String tableFille;
     String liaison;
     Fille fille;
     Vector tableauFille = new Vector();

    public Fille getFille() {
        return fille;
    }

    public void setFille(Fille fille) {
        this.fille = fille;
    }
     

    public int getIdMere() {
        return idMere;
    }

    public void setIdMere(int idMere) {
        this.idMere = idMere;
    }

    public String getTableFille() {
        return tableFille;
    }

    public void setTableFille(String tableFille) {
        this.tableFille = tableFille;
    }

    public String getLiaison() {
        return liaison;
    }

    public void setLiaison(String liaison) {
        this.liaison = liaison;
    }

    public Vector getTableauFille(Connection c) throws Exception {
        Vector tabFille = new Vector();
        Connection connexion = c;
        if(connexion == null){
            ConnectionDB con = new ConnectionDB();
            connexion = con.getConnection("Postgres");
        } 
        
            PreparedStatement stat = null;
            ResultSet res = null;
            try {
                String query = "Select * from "+this.getTableFille()+" where "+this.getLiaison()+" = ?";
                stat = connexion.prepareStatement(query);
                stat.setInt(1, this.getIdMere());
                res = stat.executeQuery();
                tabFille = this.getFille().getClass().getConstructor().newInstance().findFille(res);
                this.setTableauFille(tabFille);
            } catch (Exception ex) {
                throw ex;
            } finally {
                if (stat != null) {
                    stat.close();
                }
                if (res != null) {
                    res.close();
                }
                if (connexion != null) {
                    connexion.close();
                }
            }
            return tabFille;
    }

    public void setTableauFille(Vector tableauFille) {
        this.tableauFille = tableauFille;
    }
}
