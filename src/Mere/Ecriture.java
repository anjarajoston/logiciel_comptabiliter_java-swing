/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mere;

import Comptabiter.Erreur;
import Comptabiter.Exercice;
import Comptabiter.Mois;
import Fille.Fille;
import Fille.SousEcriture;
import java.sql.Connection;
import java.sql.Date;
import java.util.Vector;

/**
 *
 * @author User_HP
 */
public class Ecriture extends Mere{
    int id;
    int idjournal;
    int idexercice;
    Date daty;
    int etat;

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Fille getFille() {
        return fille;
    }

    public void setFille(Fille fille) {
        this.fille = fille;
    }

    public int getIdjournal() {
        return idjournal;
    }

    public void setIdjournal(int idjournal) {
        this.idjournal = idjournal;
    }

    public int getIdexercice() {
        return idexercice;
    }

    public void setIdexercice(int idexercice) {
        this.idexercice = idexercice;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public int getIdMere() {
        return idMere;
    }

    public void setIdMere(int idMere) {
        this.idMere = idMere;
    }

    public String getTableFille() {
        return tableFille;
    }

    public void setTableFille(String tableFille) {
        this.tableFille = tableFille;
    }

    public String getLiaison() {
        return liaison;
    }

    public void setLiaison(String liaison) {
        this.liaison = liaison;
    }

    public Vector getTableauFille() {
        return tableauFille;
    }

    public void setTableauFille(Vector tableauFille) {
        this.tableauFille = tableauFille;
    }
    
    public String getDateEcriture(Exercice exercice,Mois mois,int jour){
        String moisAnnee = "";
        
        int [] moisAnneeExercice = exercice.getMoisAnneeExercice(exercice);
        
        if( mois.getId() >= moisAnneeExercice[0] ){
            moisAnnee+= ((Integer)moisAnneeExercice[1]).toString()+"-"+((Integer)mois.getId()).toString()+"-"+((Integer)jour).toString();
        } else {
            moisAnnee+= ((Integer)(moisAnneeExercice[1]+1)).toString()+"-"+((Integer)mois.getId()).toString()+"-"+((Integer)jour).toString();
        }
        
        
        return moisAnnee;
    }
    
    public double [] getSommeDebitCredit(Ecriture ecriture) throws Exception{
        double [] tabSommes = new double[2];
        Connection c = null;
        Vector tableauxFilles = ecriture.getTableauFille(c);
        for( int i = 0 ; i < tableauxFilles.size() ; i++ ){
            tabSommes[0] += ((SousEcriture)tableauxFilles.elementAt(i)).getDebit();
            tabSommes[1] += ((SousEcriture)tableauxFilles.elementAt(i)).getCredit();
        }
        return tabSommes;
    }
    
    public Erreur testValidationEcriture(Ecriture ecriture) throws Exception {
            double [] debitCredit = ecriture.getSommeDebitCredit(ecriture);
            if( debitCredit[0] == debitCredit[1] ){
                return new Erreur(true," Validation accepter :) ");
            } else {
                throw new Exception(" Validation non valide! Debit n'est pas égale à Credit :( ");
            }
    }
}
