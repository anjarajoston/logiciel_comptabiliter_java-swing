/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fille;

import Mere.Mere;
import Objet.ObjetBdd;

/**
 *
 * @author User_HP
 */
public class Fille extends ObjetBdd{
    Mere mere;
    String tableMere;

    public Mere getMere() {
        return mere;
    }

    public void setMere(Mere mere) {
        this.mere = mere;
    }

    public String getTableMere() {
        return tableMere;
    }

    public void setTableMere(String tableMere) {
        this.tableMere = tableMere;
    }
}
