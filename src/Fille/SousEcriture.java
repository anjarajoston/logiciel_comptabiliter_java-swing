/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fille;

import Comptabiter.Erreur;
import Mere.Mere;

/**
 *
 * @author User_HP
 */
public class SousEcriture extends Fille{
    int id;
    int idEcriture;
    int idcompte;
    int intitule;
    String libelle;
    String reference;
    double debit;
    double credit;
    int etat;
    int compte;

    public int getCompte() {
        return compte;
    }

    public void setCompte(int compte) {
        this.compte = compte;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEcriture() {
        return idEcriture;
    }

    public void setIdEcriture(int idEcriture) {
        this.idEcriture = idEcriture;
    }

    public int getIdcompte() {
        return idcompte;
    }

    public void setIdcompte(int idcompte) {
        this.idcompte = idcompte;
    }

    public int getIntitule() {
        return intitule;
    }

    public void setIntitule(int intitule) {
        this.intitule = intitule;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public Mere getMere() {
        return mere;
    }

    public void setMere(Mere mere) {
        this.mere = mere;
    }

    public String getTableMere() {
        return tableMere;
    }

    public void setTableMere(String tableMere) {
        this.tableMere = tableMere;
    }
   
    public Erreur testInsertSousEcriture(String compte,String debit,String credit){
        if( debit.equals("") == true || credit.equals("") == true ){
            System.out.println(" débit ou crédit ne pas contennir des montant vide! ");
            return new Erreur(false," débit ou crédit ne pas contennir des montant vide! ");
        } else if( Integer.parseInt(debit) < 0 || Integer.parseInt(credit) < 0 ) {
            System.out.println(" débit ou crédit ne pas contennir des montant négatif! ");
            return new Erreur(false," débit ou crédit ne pas contennir des montant négatif! ");
        } else {
            String numeroCompte = compte.substring(0, 1);
            if ( numeroCompte.equals("7") == true && Integer.parseInt(debit) > 0 ){
                System.out.println(" compte 7 non débitable! ");
                return new Erreur(false," compte 7 non débitable! ");
            }
            else if( numeroCompte.equals("6") == true && Integer.parseInt(credit) > 0 ){
                System.out.println(" compte 6 non créditable! ");
                return new Erreur(false," compte 6 non créditable! ");
            } else {
                System.out.println(" OK! ");
                return new Erreur(true," OK! ");
            }
        }
    }
}
