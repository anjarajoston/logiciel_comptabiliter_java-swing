/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compte;

import Balance.Balance;
import Objet.ObjetBdd;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author User_HP
 */
public class Stock extends ObjetBdd{
    int idCompte = 3;
    String nomCompte = "Stock";
    int intitule;
    Double debit;
    Double credit;

    public int getIdCompte() {
        return idCompte;
    }

    public String getNomCompte() {
        return nomCompte;
    }

    public int getIntitule() {
        return intitule;
    }

    public void setIntitule(int intitule) {
        this.intitule = intitule;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }
}
