/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compte;

import Comptabiter.Calcul;
import Comptabiter.Erreur;
import Objet.ObjetBdd;
import java.sql.Date;
import java.time.LocalDate;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author User_HP
 */
public class Immobilisation extends ObjetBdd{
    int idCompte = 2;
    String nomCompte = "Immobilisation";
    int intitule;
    Double debit;
    Double credit;
    
    String libelle;
    double montant;
    int anneeAmmortissement;
    String date;
    
    boolean etat;
    double montantVente;

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public double getMontantVente() {
        return montantVente;
    }

    public void setMontantVente(double montantVente) {
        this.montantVente = montantVente;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public int getAnneeAmmortissement() {
        return anneeAmmortissement;
    }

    public void setAnneeAmmortissement(int anneeAmmortissement) {
        this.anneeAmmortissement = anneeAmmortissement;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getIdCompte() {
        return idCompte;
    }

    public String getNomCompte() {
        return nomCompte;
    }

    public int getIntitule() {
        return intitule;
    }

    public void setIntitule(int intitule) {
        this.intitule = intitule;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }
    
    public Erreur testDateCession(String date){
        if ( Date.valueOf(date).compareTo(Date.valueOf(LocalDate.now())) == -1){
            System.out.println(" Date inf now! ");
            return new Erreur(false," Date inférieur à aujourd'hui! Aujourd'hui est "+Date.valueOf(LocalDate.now()).toString());  
        } else {
            System.out.println(" OK! ");
            return new Erreur(true," OK! ");
        }
    }
    
    public Erreur testInsertImmobilisation(String libelle,String montant,String anneeAmmort,String date){
        if( libelle.equals("") == true && montant.equals("") == true && anneeAmmort.equals("") == true && date.equals("") == true ){
            System.out.println(" Champs vide! ");
            return new Erreur(false," Veuillez remplir tous les champs! ");  
        } else if(libelle.equals("") == true){
            System.out.println(" Libélle est vide! ");
            return new Erreur(false," Veuillez remplir le champ libélle! ");        
        } else if ( Integer.parseInt(montant) < 0){
            System.out.println(" Montant immo négatif! ");
            return new Erreur(false," Montant immobilisation ne pas contennir des montant négatif! ");  
        } else if ( Integer.parseInt(anneeAmmort) < 0){
            System.out.println(" Annee ammortissement négatif! ");
            return new Erreur(false," Année ammortissement ne pas contennir des montant négatif! ");  
        } else if ( date.equals("") == true){
            System.out.println(" Date immo vide! ");
            return new Erreur(false," Date immobilisation vide! ");  
        } else if ( Date.valueOf(date).compareTo(Date.valueOf(LocalDate.now())) == 1){
            System.out.println(" Date sup now! ");
            return new Erreur(false," Date supérieur à aujourd'hui! Aujourd'hui est "+Date.valueOf(LocalDate.now()).toString());  
        } else {
            System.out.println(" OK! ");
            return new Erreur(true," OK! ");
        }
    }
    
    public String [] getDateDebutAndFin(String date){
        String [] dateDebutAndFin = new String[2];
        
        String jour = date.split("-")[2];
        String mois = date.split("-")[1];
        String annee = date.split("-")[0];
        
        dateDebutAndFin[0] = date;
        dateDebutAndFin[1] = annee+"-12-31";
        
        return dateDebutAndFin;
    }
    
    public String getPrixDeVente(Immobilisation immobilisation,String [] dateDebutAndFin,int enEm){
        String valeur = "";
        try {
            int diff = new Calcul().getDifference(dateDebutAndFin,enEm);
            
            System.out.println(" diff : "+diff);
            
            double montantImmobilisation = immobilisation.getMontant();
            double pourcentage = montantImmobilisation/(immobilisation.getAnneeAmmortissement());
            double montantParJour = pourcentage/365;
            
            double montantFinal;
            if( enEm == 1 ){
                montantFinal = (montantImmobilisation)-((montantParJour)*(diff));
            } else {
                montantFinal = (pourcentage)-((montantParJour)*(diff));
            }
            
            valeur = ((Double)montantFinal).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valeur;
    } 
    
    public String getMontantAmmortAnnuel(Immobilisation immobilisation,String [] dateDebutAndFin,int enEm){
        String valeur = "";
        try {
            int diff = new Calcul().getDifference(dateDebutAndFin,enEm);
            
            System.out.println(" diff : "+diff);
            
            double montantImmobilisation = immobilisation.getMontant();
            double pourcentage = montantImmobilisation/(immobilisation.getAnneeAmmortissement());
            double montantParJour = pourcentage/365;
            
            double montantFinal = ((montantParJour)*(diff));
            
            valeur = ((Double)montantFinal).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valeur;
    } 
    
    public JTable getTableauAmmortissement(Immobilisation immobilisation){
        JTable table = new JTable();
        String dateAnio = null;
        if( immobilisation.getDate().equals("") == true ){
            dateAnio = Date.valueOf(LocalDate.now()).toString();
        } else {
            dateAnio = immobilisation.getDate();
        }
        String jourAnio = dateAnio.split("-")[2];
        String moisAnio = dateAnio.split("-")[1];
        String anneeAnio = dateAnio.split("-")[0];
        
        
        
        System.out.println(" jourAnio : "+jourAnio+" moisAnio : "+moisAnio+" anneeAnio :"+anneeAnio);
        
        String [] enTete = new String[4];
        enTete[0] = "Date";
        enTete[1] = "Montant debut d'année";
        enTete[2] = "Montant soustraite";
        enTete[3] = "Montant cumuler";
        
        String [] tabDate =  new String[immobilisation.getAnneeAmmortissement()+1];
        
        for( int i = 0 ; i < tabDate.length ; i++ ){
            if( i == 0 ) {
                String date = ((Integer)(Integer.parseInt(anneeAnio)+(i))).toString()+"-"+moisAnio+"-"+jourAnio;
                String [] dateIntervale = getDateDebutAndFin(date);
                tabDate[i] = dateIntervale[0]+"|"+dateIntervale[1];
            } else if ( i == immobilisation.getAnneeAmmortissement() ){
                tabDate[i] = ((Integer)(Integer.parseInt(anneeAnio)+(i))).toString()+"-01-01"+"|"+((Integer)(Integer.parseInt(anneeAnio)+(i))).toString()+"-"+moisAnio+"-"+jourAnio;
            } else {
                String date = ((Integer)(Integer.parseInt(anneeAnio)+(i))).toString()+"-01-01";
                String [] dateIntervale = getDateDebutAndFin(date);
                tabDate[i] = dateIntervale[0]+"|"+dateIntervale[1];
            }
        }
        
        double montantImmobilisation = immobilisation.getMontant();
        double pourcentage = montantImmobilisation/(immobilisation.getAnneeAmmortissement());
        
        String [] montantParAnnee = new String[immobilisation.getAnneeAmmortissement()];
        montantParAnnee[0] = ((Double)montantImmobilisation).toString();
        
        double montantInitial = montantImmobilisation;
        for( int i = 1 ; i < immobilisation.getAnneeAmmortissement() ; i++ ){
            montantInitial = montantInitial-pourcentage;
            montantParAnnee[i] = ((Double)(montantInitial)).toString();
        }
        
        for( int i = 0 ; i < immobilisation.getAnneeAmmortissement() ; i++ ){
            System.out.println(" enTete : "+tabDate[i]+" valeur : "+montantParAnnee[i]);
        }
        double diff = 0;
        double valDiff1 = 0;
        double diff1 = 0;
        
        String [][] valeur = new String[immobilisation.getAnneeAmmortissement()+1][4];
        
        for( int i = 0 ; i < valeur.length ; i++ ) {
            for (int j = 0; j < valeur[i].length ; j++ ) {
                switch (j) {
                    case 0:
                        valeur[i][j] = tabDate[i];
                        break;
                    case 1:
                switch (i) {
                    case 0:
                        String [] date = tabDate[i].split("\\|");
                        valeur[i][j] = montantParAnnee[i]+" Ar";
                        diff = (Double.parseDouble(montantParAnnee[i]))-(Double.parseDouble(getMontantAmmortAnnuel(immobilisation, date, 0)));
                        break;
                    case 1:
                        valeur[i][j] = ((Double)diff).toString()+" Ar";
                        valDiff1 = diff;
                        break;
                    default:
                        valDiff1 = diff1;
                        valeur[i][j] = ((Double)diff1).toString()+" Ar";
                        break;
                }
break;
                    case 2:
                        if ( i == 0 ) {
                            valeur[i][j] = ((Double)diff).toString()+" Ar";
                        } else if ( i == immobilisation.getAnneeAmmortissement() ) {
                            valeur[i][j] =" 0 Ar";
                        } else {
                            diff1 = valDiff1-pourcentage;
                            valeur[i][j] = ((Double)diff1).toString()+" Ar";
                        }   break;
                    default:
                        if ( i == 0 ) {
                            String [] date = tabDate[i].split("\\|");
                            valeur[i][j] = getMontantAmmortAnnuel(immobilisation, date, j)+" Ar";
                        } else if ( i == immobilisation.getAnneeAmmortissement() ) {
                            String [] date = tabDate[i].split("\\|");
                            valeur[i][j] = getMontantAmmortAnnuel(immobilisation, date, j)+" Ar";
                        } else {
                            valeur[i][j] = ((Double)pourcentage).toString()+" Ar";
                        }   break;
                }
            }
        }
        
        DefaultTableModel model = new DefaultTableModel(valeur,enTete);
        table.setModel(model);
        
        return table;
    }
    
}
