/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compte;

import Objet.ObjetBdd;


/**
 *
 * @author User_HP
 */
public class Achat extends ObjetBdd{
    int idCompte = 6;
    String nomCompte = "Achat";
    int intitule;
    Double debit;
    Double credit;

    public int getIdCompte() {
        return idCompte;
    }

    public String getNomCompte() {
        return nomCompte;
    }

    public int getIntitule() {
        return intitule;
    }

    public void setIntitule(int intitule) {
        this.intitule = intitule;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }
}
